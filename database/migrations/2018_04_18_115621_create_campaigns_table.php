<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable()->comment('label');
            $table->string('subject', 100)->nullable()->comment('title');
            $table->string('from_name', 100)->nullable();
            $table->string('from_email', 100)->nullable();
            $table->string('reply_to', 100)->nullable();
            $table->longtext('plain_text')->nullable();
            $table->longtext('html_text')->nullable();
            $table->longtext('query_string')->nullable();
            $table->integer('to_send')->nullable()->comment('Сколька человек получили');
            $table->integer('recipients')->default(0)->comment('получатели')->nullable();
            $table->integer('wysiwyg')->default(0)->comment('Редактор')->nullable();
            $table->string('status')->default(0)->comment('статус')->nullable();
            $table->string('timeout_check', 100)->nullable();
            $table->longtext('opens')->nullable();
            $table->integer('timezone')->nullable();
            $table->longtext('errors')->nullable();
            $table->timestamp('run_date')->nullable()->comment('дата планировщика');
            $table->timestamp('send_at')->nullable()->comment('дата отправки');
            $table->integer('complaint_setup')->default(0)->nullable()->comment('устоновка Amozon SNS спам');
            $table->integer('bounce_setup')->default(0)->nullable()->comment('устоновка Amozon SNS отскок');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
