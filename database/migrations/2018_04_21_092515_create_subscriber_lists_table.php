<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriberListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('description')->nullable();
            $table->integer('opt_in')->default(0)->nullable();
            $table->string('confirm_url', 100)->nullable();
            $table->string('subscribed_url', 100)->nullable();
            $table->string('unsubscribed_url', 100)->nullable();
            $table->integer('thankyou')->default(0)->nullable();
            $table->string('thankyou_subject', 100)->nullable();
            $table->longtext('thankyou_message')->nullable();
            $table->integer('goodbye')->default(0)->nullable();
            $table->string('goodbye_subject', 100)->nullable();
            $table->longtext('goodbye_message')->nullable();
            $table->longtext('confirmation_subject')->nullable();
            $table->longtext('confirmation_email')->nullable();
            $table->integer('unsubscribe_all_list')->default(1)->nullable();
            $table->longtext('custom_fields')->nullable();
            $table->integer('prev_count')->nullable();
            $table->integer('currently_processing')->nullable();
            $table->integer('total_records')->nullable();
            $table->timestamps();
        });
        Schema::create('subscribers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('name')->nullable();
            $table->longtext('custom_fields')->nullable();
            $table->integer('bounced')->default(0)->nullable();
            $table->integer('bounce_soft')->default(0)->nullable();
            $table->integer('unsubscribed')->default(0)->nullable();
            $table->integer('complaint')->default(0)->nullable();
            $table->integer('last_campaign')->nullable();
            $table->integer('last_ares')->nullable();
            $table->integer('confirmed')->default(1)->nullable();
            $table->text('messageID')->nullable();
            $table->timestamp('join_date')->nullable();
            $table->timestamps();
        });

        Schema::create('subscriber_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subscriber_id')->unsigned()->nullable();
            $table->foreign('subscriber_id')->references('id')->on('subscribers')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('list_id')->unsigned()->nullable();
            $table->foreign('list_id')->references('id')->on('lists')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lists');
        Schema::dropIfExists('subscribers');
        Schema::dropIfExists('subscriber_lists');
    }
}
