<?php


return [

    'name' => 'Названия',
    'email' => 'E-mail',
    'status' => 'Статус',

    'action' => [
      'create' => 'Создать',
      'delete' => 'Удалить',
      'edit' => 'Редактировать',
      'view' => 'Просмотр',
      'unsubscribe' => 'Отписаться',
      'remove' => 'Убрать',
    ],

    'date' => [
      'active'=> 'Последняя активность',
      'create' => 'Дата создания',
      'just' => 'Дата',
      'sent' => 'Отправлен',
    ],

    'recipients' => 'Получатели',
    'active' => 'Активный',
    'unsubscribers' => 'Отписался',
    'bounced' => 'Отскочила',
    'uopen' => 'Уникальные открытия',
    'uclick' => 'Уникальные клики',
    'lists' => 'Списки',


];
