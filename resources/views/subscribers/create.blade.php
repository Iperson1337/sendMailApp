@extends('layouts.app')

@section('content')
<div class="card">
  <div class="card-header">
    добавить подписчика
  </div>
  <div class="card-body">
    <form action="{{ route('subscribers.store') }}" method="post">
      {{ csrf_field() }}
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Имя</label>
            <input type="text" name="name" value="" class="form-control">
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="text" name="email" value="" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}">
            @if ($errors->has('email'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Список</label>
            <select class="form-control" name="lists[]" multiple>
              @foreach($lists as $list)
                <option value="{{$list->id}}">{{$list->name}}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>

      <button type="submit" name="button"><i class="fas fa-plus"></i> Добавить</button>
    </form>
  </div>
</div>

@stop
