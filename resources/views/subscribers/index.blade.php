@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <a href="{{route('subscribers.create')}}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Добавить подписчика">
            	<i class="fas fa-user-plus"></i>
            </a>
            <a href="javascript:void(0)" class="btn btn-success btn-sm" data-toggle="modal" data-target="#exampleModal" data-toggle="tooltip" data-placement="top" title="Добавить подписчиков через EXCEL">
            	<i class="far fa-file-excel"></i>
            </a>
        </div>
        <div class="card-body" style="padding:10px 0 10px 0;">
          <table class="table table-striped table-condensed responsive" id="utable">
              <thead>
                <tr>
                  <th style="width: 20px;">#</th>
                  <th>Имя</th>
                  <th>Email</th>
                  <th class="text-center">{{trans('app.lists')}}</th>
                  <th class="text-center" style="width: 40px;">{{trans('app.date.active')}}</th>
                  <th class="text-center" style="width: 20px;">{{trans('app.status')}}</th>
                  <th class="text-center" style="width: 20px;">{{trans('app.action.unsubscribe')}}</th>
                  <th class="text-center" style="width: 20px;">{{trans('app.action.delete')}}</th>
                </tr>
              </thead>
              <tbody>
                @foreach($subscribers as $key=>$subscriber)
                  <tr>
                    <td>{{$key+1}}</td>
                    <td><a href="{{route('subscribers.edit', $subscriber->id)}}">{{$subscriber->name}}</a></td>
                    <td><a href="{{route('subscribers.edit', $subscriber->id)}}">{{$subscriber->email}}</a></td>
                    <td class="text-center">
                        @foreach($subscriber->lists as $list)
                            <span class="badge badge-primary">{{$list->name}}</span>
                        @endforeach
                    </td>
                    <td>{{date('d.m.Y H:i', strtotime($subscriber->updated_at))}}</td>
                    <td class="text-center">
                      @if($subscriber->bounced === 1)
                        <span class="badge badge-dark">Отскочила</span>
                      @elseif($subscriber->complaint === 1)
                        <span class="badge badge-dark">Отмечено как спам</span>
                      @elseif($subscriber->unsubscribed === 1)
                        <span class="badge badge-danger">Отписался</span>
                      @elseif($subscriber->confirmed === 0)
                        <span class="badge badge-warning">Неподтвержденный</span>
                      @else
                        <span class="badge badge-success">Подписан</span>
                      @endif
                    </td>
                    <td class="text-center">
                      @if($subscriber->bounced === 1 || $subscriber->complaint === 1)
                        -
                      @else
                        @if($subscriber->unsubscribed === 0 || is_null($subscriber->unsubscribed))
                          <a href="{{route('subs.unsubscribe', $subscriber->id)}}" style="color: red">
                            <i class="fas fa-ban" data-toggle="tooltip" data-placement="top" title="отписаться"></i>
                          </a>
                        @else
                          <a href="{{route('subs.subscribe', $subscriber->id)}}">
                            <i class="fas fa-check" data-toggle="tooltip" data-placement="top" title="подписка"></i>
                          </a>
                        @endif
                      @endif
                    </td>
                    <td class="text-center">
                        <a id="subscriber-delete" data-href="{{route('subscribers.destroy', $subscriber->id)}}"><i class="far fa-trash-alt" style="color: #000;"></i></a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
          </table>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <form action="{{route('subs.import')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Добавить подписчиков через EXCEL</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                Нужно указать <code>email</code> и <code>name</code> на первом строке в excel
                <div class="form-group">
                  <input type="file" name="file" value="" class="form-control">
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
              <button type="submit"class="btn btn-success" >Импортировать</button>
            </div>
          </form>
        </div>
      </div>
    </div>
@endsection
@push('js')
  @if(Session::has('errors'))
      <script>
          swal({
            title: "Ошибка!",
            text: "@foreach($errors->all() as $error) {{ $error }} @endforeach",
            icon: "error",
          })
      </script>
  @endif
  <script>
      $(document).on('click', '#subscriber-delete', function(e){
        var url = $(this).attr('data-href');
        subscribeDelete(url);
        e.preventDefault();
      });

      function subscribeDelete(url){
        swal({
            title: "Вы уверены?",
            text: "Вы действительно хотите удалить этого подписчика",
            icon: "warning",
            buttons: {
              cancel: "Отмена",
              confirm: "Да",
            },
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
              return new Promise(function(resolve) {
                 $.ajax({
                    url: url,
                    type: 'POST',
                    data: {_token: '{{csrf_token()}}', _method: 'DELETE'},
                    dataType: 'json',
                    success: function(data) {
                      swal('Удален!', data.message, 'success');
                      location.reload();
                    },
                    error: function(){
                      swal('Упсс...', 'Что-то пошло не так с ajax!', 'error');
                    }
                 });
              });
            }
        });
      }
      $(function () {
        $('[data-toggle="tooltip"]').tooltip()
      })
  </script>
@endpush
