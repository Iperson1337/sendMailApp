@extends('layouts.app')

@section('content')
<div class="card">
  <div class="card-header">
    добавить подписчика
  </div>
  <div class="card-body">
    <form action="{{ route('subscribers.update', $subscriber->id) }}" method="post">
      {{ csrf_field() }}
      {{method_field('PUT')}}
      <div class="form-group row">
        <div class="col-md-6">
          <label>Имя</label>
          <input type="text" name="name" value="{{$subscriber->name}}" class="form-control">
        </div>
        <div class="col-md-6">
          <label>Email</label>
          <input type="text" name="email" value="{{$subscriber->email}}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}">
          @if ($errors->has('email'))
              <span class="invalid-feedback">
                  <strong>{{ $errors->first('email') }}</strong>
              </span>
          @endif
        </div>
      </div>
      <div class="form-group">
        <label>Список</label>
        <select id="lists" class="form-control" name="lists[]" multiple>
          @foreach($lists as $list)
            <option value="{{$list->id}}" >{{$list->name}}</option>
          @endforeach
        </select>
      </div>
      <button type="submit" name="button" class="btn btn-primary"><i class="fas fa-check"></i> Сохранить</button>
    </form>
  </div>
</div>

@endsection
@push('js')
<script>
    $(document).ready(function() {
        $('#lists').select2();
        $('#lists').select2().val({!! json_encode($subscriber->lists()->allRelatedIds()) !!}).trigger('change');
    });
</script>
@endpush
