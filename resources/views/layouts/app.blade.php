<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <script src="{{ asset('js/app.js') }}" ></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.2.1/dt-1.10.16/datatables.min.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
    <style>
      .nav-pills .nav-link {
          border-radius: 0;
      }
    </style>
    @stack('style')
</head>
<body style="background: #f9f9f9">
    <div id="app">
        @include('_includes.nav')
        <main class="py-4">
          <div class="container-fluid">
            <div class="row justify-content-center">
                @auth
                  <div class="col-12 col-sm-5 col-md-2">
                      @include('_includes.sidebar')
                      @include('_includes.aws_ses_quota')
                  </div>
                @endauth
                <div class="col-12 col-sm-7 col-md-10">
                  @yield('content')
                </div>
            </div>
        </main>
    </div>


    <!-- Scripts -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="{{ asset('js/ckeditor/ckeditor.js?7') }}" ></script>
    <script src="{{ asset('js/ckeditor/lang/ru.js?t=G14E') }}" ></script>
    <script src="{{ asset('js/ckeditor/styles.js?t=G14E') }}" ></script>
    <script src="{{ asset('js/ckeditor/plugins/codemirror/plugin.js?t=G14E') }}" ></script>
    <script src="{{ asset('js/ckeditor/plugins/codemirror/lang/ru.js?t=G14E') }}" ></script>
    <script src="{{ asset('js/highcharts/highcharts.js') }}" ></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
      $(function () {
        $('[data-toggle="tooltip"]').tooltip()
      })
    </script>
    <script>
        $('#utable').dataTable( {
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Russian.json"
            }
        });
    </script>
    @if(Session::has('status'))
        <script>
            swal("Успешно выполнено!", "{{ session('status') }}", "success");
        </script>
    @endif
    @if(Session::has('error'))
        <script>
            swal({
              title: "Ошибка!",
              text: "{{ session('error') }}",
              icon: "error",
            })
        </script>
    @endif
    @stack('js')
</body>
</html>
