@php
		$route = Route::currentRouteName();
		if(!empty(config('services.ses.key')) && !empty(config('services.ses.secret'))){
			$client = \AWS::createClient('ses');
			try {
					$result = $client->getSendQuota([]);
			} catch (Aws\Exception\AwsException $e) {
					$result = '';
			}
		}else{
			$result = '';
		}

@endphp
@if(!empty($result))
  <div class="nav flex-column nav-pills mt-4 mb-4">
    <div class="card">
			<div class="card-header text-white" style="background: #febd69;">
				<strong>Amazon SES Quota</strong>
			</div>
			<a class="pl-3 pb-1 pt-2 border-bottom">
				SES Region:
				<span class="badge badge-secondary">{{config('services.ses.region')}}</span>
			</a>
			<a class="pl-3 pb-1 border-bottom">
				Макс. в 24 часа:<br>
				<span class="badge badge-secondary">{{$result['Max24HourSend']}}</span>
			</a>
			<a class="pl-3 pb-1 border-bottom">
				Макс. скорость отправки:<br>
				<span class="badge badge-secondary">{{$result['MaxSendRate']}} в секунду</span>
			</a>
			<a class="pl-3 pb-1 border-bottom">
				Отправлено последние 24 часа:<br>
				<span class="badge badge-secondary">{{$result['SentLast24Hours']}}</span>
			</a>
			<a class="pl-3 pb-2 border-bottom">
				Осталось: <span class="badge badge-secondary">{{$result['Max24HourSend']-$result['SentLast24Hours']}}</span>
			</a>
    </div>
  </div>
@endif
