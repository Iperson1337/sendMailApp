@php
	$route = Route::currentRouteName();
@endphp
  <div class="nav flex-column nav-pills" id="v-pills-tab">
    <div class="card">
      <a class="nav-link {{($route == 'home') || ( $route == 'campaigns.index') ? 'active' : ''}}" href="{{route('home')}}"><i class="fab fa-telegram-plane"></i>  Рассылки</a>
      <a class="nav-link {{($route == 'templates.index') ? 'active' : ''}}" href="{{route('templates.index')}}"><i class="fas fa-envelope"></i>  Шаблоны</a>
      <a class="nav-link {{($route == 'lists.index') ? 'active' : ''}}" href="{{route('lists.index')}}"><i class="fas fa-list-ul"></i>  Список</a>
      <a class="nav-link {{($route == 'subscribers.index') ? 'active' : ''}}" href="{{route('subscribers.index')}}"><i class="fas fa-users"></i>  Подписчики</a>
      <a class="nav-link {{($route == 'settings') ? 'active' : ''}}" href="{{route('settings')}}"><i class="fas fa-cog"></i>  Настройки</a>
      <a class="nav-link {{($route == 'campaigns.reports') ? 'active' : ''}}" href="{{route('campaigns.reports')}}"><i class="far fa-chart-bar"></i>  Отчеты</a>
    </div>
  </div>
