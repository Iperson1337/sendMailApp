<div class="form-group row">
  <div class="col-sm-5">
    <strong>Ссылка с тэгом(только в HTML)</strong><br>
    <code>{{_('<unsubscribe>Unsubscribe here</unsubscribe>')}}</code>
  </div>
  <div class="col-sm-4">
    <strong>Чистая ссылка для отписки</strong><br>
    <code>[unsubscribe]</code>
  </div>
  <div class="col-sm-2">
    <strong>Email</strong><br>
    <code>[Email]</code>
  </div>
  <div class="col-sm-3">
    <strong>Имя подписчика</strong><br>
    <code>[Name,fallback=]</code>
  </div>
</div>
