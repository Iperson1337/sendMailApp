@extends('layouts.app')
@push('style')
  <style media="screen">
    .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
        color: #495057;
        background-color: #fff;
        border-color: #dee2e6 #dee2e6 #fff;
      }
  </style>
@endpush
@section('content')
<div class="card" style="border: 0;">
  <div class="card-header pb-0 pl-0" style="background-color: #f9f9f9; border-bottom: 0;">
    <ul class="nav nav-tabs border-bottom-0" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link {{ session('show')=='main' ? 'active' : '' }} {{is_null(session('show')) ? 'active' : ''}}" id="main-tab" data-toggle="tab" href="#main" role="tab" aria-controls="main" aria-selected="true">Основные настройки</a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ session('show')=='mail' ? 'active' : '' }}" id="mail-tab" data-toggle="tab" href="#mail" role="tab" aria-controls="mail" aria-selected="false">Настройки поставщика почты</a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ session('show')=='personal' ? 'active' : '' }}" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Персональные настройки</a>
      </li>
    </ul>
  </div>
  <div class="card-body border-top-0" style="border: 1px solid rgba(0,0,0,.125); border-radius: 0px 0px 0.25rem 0.25rem;">
    <div class="tab-content" id="myTabContent">
      <div class="tab-pane fade {{is_null(session('show')) ? 'show active' : ''}}" id="main" role="tabpanel" aria-labelledby="main-tab">
        <form action="{{route('settings.main')}}" method="post">
            @csrf
            <div class="form-group">
              <label>Компания</label>
              <input type="text" name="company_name" value="{{$company['company_name']}}" class="form-control{{ $errors->has('company_name') ? ' is-invalid' : '' }}">
              @if ($errors->has('company_name'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('company_name') }}</strong>
                  </span>
              @endif
            </div>
            <div class="form-group">
              <label>From Name</label>
              <input type="text" name="from_name" value="{{$company['from_name']}}" class="form-control{{ $errors->has('from_name') ? ' is-invalid' : '' }}">
              @if ($errors->has('from_name'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('from_name') }}</strong>
                  </span>
              @endif
            </div>
            <div class="form-group">
              <label>From Email</label>
              <input type="text" name="from_email" value="{{$company['from_email']}}" class="form-control mb-2{{ $errors->has('from_email') ? ' is-invalid' : '' }}" id="from_email">
              @if ($errors->has('from_email'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('from_email') }}</strong>
                  </span>
              @endif
              <p id="verification-check-loader" style="display:none;">
                <img src="{{asset('/img/loader.gif')}}" style="width:16px;"/> Проверка, подтвержден ли ваше сообщение «email» на вашей консоли SES.<br/><br/>
              </p>
              <div class="alert alert-danger" id="unverified-email" style="display:none;">
                <strong><i class="icon icon-warning-sign"></i> Неподтвержденный Email</strong>: ваш «Email» или его домен не проверен на вашей консоли Amazon SES.<br/>
                  <ul>
                    <!-- <li id="click-to-verify-copy"><a href="javascript:void(0);" id="click-to-verify-btn">Нажмите здесь, чтобы подтвердить этот «Email», Amazon отправит электронное письмо с подтверждением →</a></li> -->
                    <li><a href="https://console.aws.amazon.com/ses/home?#verified-senders-domain:" target="_blank">Проверьте этот «Email» на своей консоли Amazon SES →</a></li>
                  </ul>
              </div>
              <div class="alert alert-danger" id="unverified-email-pending" style="display:none;">
                <strong><i class="icon icon-warning-sign"></i> Электронный почто в ожидании проверки</strong>: «Ваш адрес электронной почты» или его домен находится в ожидании проверки на вашей консоли Amazon SES. Завершите проверку.
              </div>
              <div class="alert alert-danger" id="api-error" style="display:none;">
                <strong><i class="icon icon-warning-sign"></i> <?php echo _('Unable to communicate with Amazon SES API');?></strong>: <?php echo _('Please check the error message on the left for instructions.');?></div>
              <div class="alert alert-danger" id="emailvalidate" style="display:none;">
                Адрес электронной почты должен быть действительным адресом электронной почты.
              </div>
              <div class="alert alert-success" id="verified-email" style="display:none;">
                <strong><i class="icon icon-ok"></i> <?php echo _('Поздравляю! Это email подтвержден');?></strong>
              </div>
            </div>
            <div class="form-group">
              <label>Reply to Email</label>
              <input type="text" name="reply_email" value="{{$company['reply_email']}}" class="form-control{{ $errors->has('reply_email') ? ' is-invalid' : '' }}">
              @if ($errors->has('reply_email'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('reply_email') }}</strong>
                  </span>
              @endif
            </div>
            <button type="submit" name="button" class="btn btn-primary"><i class="fas fa-check"></i> Сохранить</button>
        </form>
      </div>
      <div class="tab-pane fade {{ session('show')=='mail' ? 'show active' : '' }}" id="mail" role="tabpanel" aria-labelledby="mail-tab">
        <form class="" action="{{route('settings.mail')}}" method="post">
            @csrf
            <div class="form-group">
              <label>AWS Access Key ID</label>
              <input type="text" name="key" value="{{env('SES_KEY')}}" class="form-control">
            </div>
            <div class="form-group">
              <label>AWS Secret Access Key</label>
              <input type="text" name="secret" value="{{env('SES_SECRET')}}" class="form-control">
            </div>
            <div class="form-group">
              <label>AWS Region</label>
              <select class="form-control" name="region">
                @foreach($regions as $region)
                  <option value="{{$region['value']}}" @if($region['value']===config('services.ses.region') ) selected @endif>{{$region['name']}}</option>
                @endforeach
              </select>
            </div>
            <button type="submit" name="button" class="btn btn-primary"><i class="fas fa-check"></i> Сохранить</button>
        </form>
      </div>
      <div class="tab-pane fade {{ session('show')=='personal' ? 'show active' : '' }}" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <form action="{{route('settings.personal')}}" method="post">
            @csrf
            <div class="form-group">
              <label>Имя</label>
              <input type="text" name="name" value="{{Auth::user()->name}}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}">
              @if ($errors->has('name'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
              @endif
            </div>
            <div class="form-group">
              <label>Email</label>
              <input type="text" name="email" value="{{Auth::user()->email}}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}">
              @if ($errors->has('email'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
            </div>
            <div class="form-group">
              <label>Старый Пароль</label>
              <input type="password" name="old_password" value="" class="form-control">
            </div>
            <div class="form-group">
              <label>Пароль</label>
              <input type="password" name="password" value="" class="form-control">
            </div>
            <button type="submit" name="button" class="btn btn-primary"><i class="fas fa-check"></i> Сохранить</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection
@push('js')
<script type="text/javascript">
$(document).ready(function() {
  $("#from_email").focusout(function(){
    $("#verification-check-loader").show();
    $("#unverified-email").hide();
    $("#unverified-email-pending").hide();
    $("#api-error").hide();
    $("#verified-email").hide();
    $("#emailvalidate").hide();
    $.ajax({
       url: "{{route('email.verification')}}",
       type: 'POST',
       data: { from_email: $("#from_email").val(), _token: '{{csrf_token()}}', auto_verify: 'no'},
       dataType: 'json',
       error: function(err){
         console.log(err)
         if(err.status == 422){
           $("#verification-check-loader").hide();
           $("#unverified-email").hide();
           $("#unverified-email-pending").hide();
           $("#api-error").hide();
           $("#emailvalidate").show();
         }
           if(err.responseText=='unverified'){
             $("#verification-check-loader").hide();
             $("#unverified-email").show();
             $("#unverified-email-pending").hide();
             $("#api-error").hide();
             $("#verified-email").hide();
           }else if(err.responseText=='pending verification'){
             $("#verification-check-loader").hide();
             $("#unverified-email").hide();
             $("#unverified-email-pending").show();
             $("#api-error").hide();
             $("#verified-email").hide();
           }else if(err.responseText=='verified'){
             $("#verification-check-loader").hide();
             $("#unverified-email").hide();
             $("#unverified-email-pending").hide();
             $("#api-error").hide();
             $("#verified-email").show();
           }else if(err.responseText=="api error"){
             $("#verification-check-loader").hide();
             $("#unverified-email").hide();
             $("#unverified-email-pending").hide();
             $("#api-error").show();
             $("#verified-email").hide();
           }else{
             $("#verification-check-loader").hide();
           }
       }
    });
    // $.post("{{route('email.verification')}}", { from_email: $("#from_email").val(), _token: '{{csrf_token()}}', auto_verify: 'no'},
    //   function(data) {
    //       if(data=='unverified'){
    //         $("#verification-check-loader").hide();
    //         $("#unverified-email").show();
    //         $("#unverified-email-pending").hide();
    //         $("#api-error").hide();
    //         $("#verified-email").hide();
    //       }else if(data=='pending verification'){
    //         $("#verification-check-loader").hide();
    //         $("#unverified-email").hide();
    //         $("#unverified-email-pending").show();
    //         $("#api-error").hide();
    //         $("#verified-email").hide();
    //       }else if(data=='verified'){
    //         $("#verification-check-loader").hide();
    //         $("#unverified-email").hide();
    //         $("#unverified-email-pending").hide();
    //         $("#api-error").hide();
    //         $("#verified-email").show();
    //       }else if(data=="api error"){
    //         $("#verification-check-loader").hide();
    //         $("#unverified-email").hide();
    //         $("#unverified-email-pending").hide();
    //         $("#api-error").show();
    //         $("#verified-email").hide();
    //       }else{
    //         $("#verification-check-loader").hide();
    //       }
    //   }
    // );
  });
});
</script>
@endpush
