@extends('layouts.app')
@push('style')
  <style>
    .preview {
      overflow: scroll; /* Добавляем полосы прокрутки */
      height: 600px; /* Высота блока */
      padding: 5px;
      border: solid 1px white; /* Параметры рамки */
      background: white;
    }
  </style>
@endpush
@section('content')

      <div class="row">
        <div class="col-md-4">
          <h4>Проверить отправку этой кампании</h4>
          <form action="{{route('campaigns.testsend', $campaign->id)}}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
              <label>Test email(s)</label>
              <input type="email" name="email" value="" class="form-control" required>
            </div>
            <button type="submit" class="btn btn-secondary btn-sm"><i class="far fa-envelope"></i> Отправить тестовый рассылку</button>
          </form>
          <form action="{{route('campaigns.send', $campaign->id)}}" method="post">
            {{ csrf_field() }}
            <h4 style="margin-top:30px;">Определение получателей</h4>
            <div class="form-group">
              <label>Выберите список адресов электронной почты</label>
              <select id="sublists" class="form-control{{ $errors->has('lists') ? ' is-invalid' : '' }}" multiple name="lists[]" >
                @foreach($lists as $list)
                  <option id="list-{{$list->id}}" value="{{$list->id}}" data-count="{{$list->subscribers->count()}}">
                    {{$list->name}} (<span>{{$list->subscribers->count()}}</span>)
                  </option>
                @endforeach
              </select>
              <strong>Получатели: </strong><span id="recipients">0</span> из {{$ses['Max24HourSend']-$ses['SentLast24Hours']}}<br>
              <strong>Осталось:</strong>
              <span>
                {{$ses['Max24HourSend']-$ses['SentLast24Hours']}} из {{$ses['Max24HourSend']}}
              </span>
              @if ($errors->has('lists'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('lists') }}</strong>
                  </span>
              @endif
            </div>
            <button type="submit" name="submit" value="now" class="btn btn-success" id="send-now-btn">
              <i class="fas fa-check"></i> Отправить сейчас
            </button><br>

            <div class="btn-group">
              <a href="javascript:void(0)" id="show-schedule-btn" class="mt-2"><i class="far fa-clock"></i> Запланировать</a>
              <a href="javascript:void(0)" id="btn-back" class="mt-2" style="display: none;"><i class="fas fa-arrow-left"></i> назад</a><br>
            </div>
            <div class="card" id="schedule" style="display: none;">
              <div class="card-body">
                <h4><i class="fas fa-clock"></i> Расписание этой рассылки</h4>
                <div class="form-group">
                  <label>Выберите дату</label>
                  <input class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" type="date" name="date" value="{{date('Y-m-d')}}">
                  @if ($errors->has('date'))
                      <span class="invalid-feedback">
                          <strong>{{ $errors->first('date') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="form-group">
                  <label>Выберите время</label>
                  <input class="form-control{{ $errors->has('time') ? ' is-invalid' : '' }}" type="time" name="time" value="{{date('H:i')}}">
                  @if ($errors->has('time'))
                      <span class="invalid-feedback">
                          <strong>{{ $errors->first('time') }}</strong>
                      </span>
                  @endif
                </div>
                <button type="submit" name="submit" value="later" class="btn btn-success btn-sm"><i class="fas fa-clock"></i> Запланировать сейчас</button>
              </div>
            </form>
          </div>

          <div class="edit">
            <a href="{{route('campaigns.edit', $campaign->id)}}" class="mt-2"><i class="far fa-edit"></i> Редактировать</a>
          </div>
        </div>

        <div class="col-md-8">
            <h4>Предварительный просмотр</h4>
            <blockquote style="border-left: 5px solid #c7c7c7;padding-left: 10px;">
                <strong>От</strong> <span class="badge badge-secondary">{{$campaign->from_name}} &lt;{{$campaign->from_email}}&gt;</span>
            </blockquote>
            <blockquote style="border-left: 5px solid #c7c7c7;padding-left: 10px;">
                <strong>Subject</strong> <span class="badge badge-secondary">{{$campaign->subject}}</span>
            </blockquote>
            <div class="card">
              <div class="preview">
                {!! $campaign->html_text !!}
              </div>
            </div>
        </div>
      </div>
@endsection

@push('js')
    <script>
      $("select#sublists").change(function () {
        var count = 0;
        $("select#sublists option:selected").each(function () {
              count += Number($(this).data('count'));
        });
        $("#recipients").text(count);
      });
    </script>
    <script type="text/javascript">
      $('#schedule').hide();
      $('#btn-back').hide();
      $('#show-schedule-btn').click(function () {
        $("#schedule").slideDown("fast");
        $('#btn-back').show();
        $('#show-schedule-btn').hide();
        $('#send-now-btn').hide();
        $('#send-now-btn').attr('disabled', 'disabled');
      });
      $('#btn-back').click(function () {
        $("#schedule").slideUp("fast");
        $('#btn-back').hide();
        $('#show-schedule-btn').show();
        $('#send-now-btn').show();
        $('#send-now-btn').removeAttr('disabled');;
      });
      $('#show-schedule-btn').click(function () {
        $("#schedule").slideDown("fast");
        $('#btn-back').show();
        $('#show-schedule-btn').hide();
        $('#send-now-btn').hide();
        $('#send-now-btn').attr('disabled', 'disabled');
      });
    </script>
  @if ($errors->has('time'))
      <script>
          $("#schedule").slideDown("fast");
          $('#btn-back').show();
          $('#show-schedule-btn').hide();
          $('#send-now-btn').hide();
          $('#send-now-btn').attr('disabled', 'disabled');
      </script>
  @endif
  @if ($errors->has('date'))
      <script>
          $("#schedule").slideDown("fast");
          $('#btn-back').show();
          $('#show-schedule-btn').hide();
          $('#send-now-btn').hide();
          $('#send-now-btn').attr('disabled', 'disabled');
      </script>
  @endif
@endpush
