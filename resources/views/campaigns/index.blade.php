@extends('layouts.app')

@section('content')
      <div class="card">
        <div class="card-header">
            <a href="{{route('campaigns.create')}}" class="btn btn-primary">
                <i class="fas fa-plus-circle"></i> Создать рассылку
            </a>
        </div>
        <div class="card-body" style="padding: 10px 0 10px 0;">
            <table class="table table-hover" id="utable">
              <thead>
                  <tr>
                      <th style="width: 20px;">#</th>
                      <th>{{ trans('app.name') }}</th>
                      <th class="text-center">{{ trans('app.recipients') }}</th>
                      <th class="text-center">{{ trans('app.date.sent') }}</th>
                      <th class="text-center">{{ trans('app.uopen') }}</th>
                      <th class="text-center">{{ trans('app.uclick') }}</th>
                      <th class="text-center">Дублировать</th>
                      <th class="text-center">{{ trans('app.action.delete') }}</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($campaigns as $k=>$campaign)
                    <tr>
                        <td>{{$k+1}}</td>
                        <td>
                          <?php $tooltip = '' ?>
                          @if($campaign->isDraft())
                            <?php $tooltip = 'Определение получателей и отправка' ?>
                            <span class="badge badge-secondary">Черновик</span>
                          @elseif($campaign->isQueue())
                            <?php $tooltip = 'Запланировано '.date('d.m.Y H:i', strtotime($campaign->run_date)) ?>
                            <span class="badge badge-info">Запланирован</span>
                          @elseif($campaign->isDone())
                            <?php $tooltip = 'Посмотреть отчет' ?>
                            <span class="badge badge-success">Отправлен</span>
                          @elseif($campaign->isSending())
                            <?php $tooltip = 'Подготовка к отправке' ?>
                            <span class="badge badge-warning">Подготовка</span>
                          @endif
                            <a @if(!$campaign->isSending()) href="{{route('campaigns.show',$campaign->id)}}" @endif data-toggle="tooltip" data-placement="top" title="{{$tooltip}}" >
                              {{$campaign->name or $campaign->subject}}
                            </a>
                          @if($campaign->isDraft())
                            | <a href="{{route('campaigns.edit',$campaign->id)}}" class="text-secondary">Редактировать</a></td>
                          @endif
                        </td>
                        <td class="text-center" id="progress-{{$campaign->id}}">
                          @if(empty($campaign->recipients))
                            -
                          @elseif($campaign->isSending())
                            Проверка...
                          @else
                            {{$campaign->recipients}}
                          @endif
                        </td>
                        <td class="text-center">{{empty($campaign->send_at)? '-' : date('d.m.Y H:i',strtotime($campaign->send_at))}}</td>
                        <td class="text-center">
                          @empty($campaign->send_at)
                            -
                          @else
                            <span class="badge badge-secondary">
                              {{$opens[$campaign->id]['per']}}%
                            </span> {{number_format($opens[$campaign->id]['unique'])}} открыт
                          @endempty
                        </td>
                        <td class="text-center">
                          @empty($campaign->send_at)
                            -
                          @else
                            <span class="badge badge-secondary">{{$percentage_clicked[$campaign->id]}}%</span>
                            {{number_format(App\Campaign::get_click_percentage($campaign->id))}} клик
                          @endempty
                        </td>
                        <td class="text-center">
                          <a href="{{route('campaigns.dublicate', $campaign->id)}}"><i class="fas fa-clone"></i></a>
                        </td>
                        <td class="text-center">
                            <form action="{{action('CampaignController@destroy', $campaign->id)}}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-link"><i class="far fa-trash-alt" style="color: red;"></i></button>
                            </form>
                        </td>
                    </tr>
                    @if($campaign->isSending())
                      <script>
                          $(document).ready(function() {
                            refresh_interval = setInterval(function(){get_sent_count('{{$campaign->id}}')}, 2000);

                            function get_sent_count(cid)
                            {
                              clearInterval(refresh_interval);

                                $.post("{{route('campaigns.progress')}}", { campaign_id: cid, _token: '{{csrf_token()}}' },
                                function(data) {
                                    if(data)
                                    {
                                      if(data.indexOf("%)") != -1)
                                        refresh_interval = setInterval(function(){get_sent_count('{{$campaign->id}}')}, 2000);

                                        $("#progress-{{$campaign->id}}").html(data);

                                      if(data != "0 <span style=\"color:#488846;\">(0%)</span> <img src=\"{{asset('img/loader.gif')}}\" style=\"width:16px;\"/>")
                                      {
                                        window.location = "{{route('home')}}";
                                      }
                                    }
                                    else
                                    {
                                      $("#progress-{{$campaign->id}}").html("Error retrieving count");
                                    }
                                }
                              );
                            }
                          });
                      </script>
                    @endif
                @endforeach
              </tbody>
            </table>
        </div>

    </div>
@endsection
