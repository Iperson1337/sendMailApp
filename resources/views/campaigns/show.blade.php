@extends('layouts.app')
@push('style')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
@endpush
@section('content')
  <h2>Отчет по email рассылке</h2>
  <span>Названия: {{$campaign->name}}</span><br>
  <h3>Тема: {{$campaign->subject}}
    <a data-fancybox data-type="iframe" data-src="{{route('campaigns.preview', $campaign->id)}}" href="javascript:;" title="Просмотр тело рассылки" style="color: #41c3f5;">
      <span class="fa fa-eye"></span>
    </a>
  </h3>
  <p style="margin-bottom: 5px;">
    <em>Отправлен в {{date('d.m.Y',strtotime($campaign->send_at))}}
      <span class="badge badge-success">{{$campaign->recipients}} подписчикам</span>
    </em>
  </p>
  <p>
    <em>Списки
      @foreach($campaign->lists as $list)
        <span class="badge badge-secondary">{{$list->name}}</span>
      @endforeach
    </em>
  </p>
  <div class="row">
    <div class="col-md-12">
      <div id="container"></div>
    </div>
  </div>
  <div class="row mb-4" style="margin-top: 20px;">
    <div class="col-md-6">
      <div class="card">
        <div class="card-body">
          <h3>
            <span class="badge badge-success">
              {{$campaignOpen['percentage']}}%
            </span> Открыто
            <span class="badge badge-secondary" style="font-size: 50%; vertical-align: middle;">
              {{$campaignOpen['unique']}} уникально  / открыто {{$campaignOpen['all']}} раз
            </span>
          </h3>
          <h3><span class="badge badge-warning">{{$campaignOpen['not'] }}</span> Не открыта</h3>
          <h3>
            <span class="badge badge-primary">
              {{$campaignOpen['click_percentage']}}%
            </span> Клик по ссылке
            <span class="badge badge-secondary" style="font-size: 50%; vertical-align: middle;">
              {{$campaignOpen['click']}} уникальных кликов
            </span>
          </h3>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card">
        <div class="card-body">
          <h3>
            <span class="badge badge-danger">{{ $subscriberPercentage['unsubscribe'] }}%</span> Отписался
            <span class="badge badge-secondary" style="font-size: 50%; vertical-align: middle;">{{ $subscriberCount['unsubscribes'] }} отписался</span>
          </h3>
          <h3>
            <span class="badge badge-dark">{{$subscriberPercentage['bounced'] }}%</span> Отклонено
            <span class="badge badge-secondary" style="font-size: 50%; vertical-align: middle;">{{ $subscriberCount['bounced'] }} отклонено</span>
          </h3>
          <h3>
            <span class="badge badge-dark">{{$subscriberPercentage['complaint']}}%</span> Помечены как спам
            <span class="badge badge-secondary" style="font-size: 50%; vertical-align: middle;">{{ $subscriberCount['complaints'] }} помечены как спам</span>
          </h3>
        </div>
      </div>
    </div>
  </div>

  <div class="card mb-4">
    <div class="card-header bg-transparent border-primary">
        <h4 class="card-title">Активность ссылки</h4>
    </div>
    <div class="row">
      <div class="col-md-12">
        <table class="table table-striped table-hover" id="clicks" role="grid">
  			  <thead>
  			    <tr role="row" class="tablesorter-headerRow">
  			      <th>Link (URL)</th>
  			      <th>Уникальный</th>
  			      <th>Всего</th>
  			    </tr>
  			  </thead>
  			  <tbody>
            @foreach($campaign->links as $link)
    	  			<tr role="row">
    			      <td><a href="{{$link->link}}" target="_blank" data-original-title="" title="">{{$link->link}}</a></td>
    			      <td>{{empty($link->clicks) ? 0 :  count(array_unique(explode(',', $link->clicks)))}}</td>
    			      <td>{{empty($link->clicks)? 0 : count(explode(',', $link->clicks))}}</td>
    			    </tr>
            @endforeach
  			  </tbody>
  			</table>
      </div>
    </div>
  </div>

  <div class="card mb-4">
    <div class="card-header bg-transparent border-success">
        <h3>Последние 10 открытых</h3>
    </div>
    <div class="row">
      <div class="col-md-12">
        <table class="table table-striped" id="clicks" role="grid">
          <thead>
            <tr role="row" class="tablesorter-headerRow">
              <th>Имя</th>
              <th>Email</th>
              <th>Список</th>
              <th>Статус</th>
            </tr>
          </thead>
          <tbody>
            @if(count($subscribers)>0)
              @foreach($subscribers as $subscriber)
                  <tr role="row">
                    <td>{{$subscriber->name}}</td>
                    <td>{{$subscriber->email}}</td>
                    <td>
                      @foreach($subscriber->lists as $list)
                        <span class="badge badge-pill badge-secondary">{{$list->name }}</span>
                      @endforeach
                    </td>
                    <td>
                      @if($subscriber->unsubscribed==1)
                        <span class="badge badge-danger">Отписался</span>
                      @elseif($subscriber->complaint==1)
                        <span class="badge badge-dark">Отмечено как спам</span>
                      @elseif($subscriber->bounced==1)
                        <span class="badge badge-dark">Отклонил</span>
                      @else
                        <span class="badge badge-success">Подписан</span>
                      @endif
                    </td>
                  </tr>
              @endforeach
            @else
              <tr>
                <td colspan="5" class="text-center"><h5>Нет данных</h5></td>
              </tr>
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="card mb-4">
    <div class="card-header bg-transparent border-danger">
        <h3>Последние 10 отписавшихся</h3>
    </div>
    <div class="row">
      <div class="col-md-12">
        <table class="table table-striped table-condensed responsive tablesorter tablesorter-default hasSaveSort" id="clicks" role="grid">
          <thead>
            <tr role="row" class="tablesorter-headerRow">
              <th>Имя</th>
              <th>Email</th>
              <th>Список</th>
              <th>Статус</th>
              <th>Дата</th>
            </tr>
          </thead>
          <tbody>
            @if(count($subscribers->where('unsubscribed', '1')) > 0)
              @foreach($subscribers->where('unsubscribed', '1') as $subscriber)
                  <tr role="row">
                    <td>{{$subscriber->name}}</td>
                    <td>{{$subscriber->email}}</td>
                    <td>
                      @foreach($subscriber->lists as $list)
                        <span class="badge badge-pill badge-secondary">{{$list->name }}</span>
                      @endforeach
                    </td>
                    <td>
                      @if($subscriber->unsubscribed==1)
                        <span class="badge badge-danger">Отписался</span>
                      @elseif($subscriber->complaint==1)
                        <span class="badge badge-dark">Отмечено как спам</span>
                      @elseif($subscriber->bounced==1)
                        <span class="badge badge-dark">Отклонил</span>
                      @elseif($subscriber->unsubscribed==0)
                        <span class="badge badge-success">Подписан</span>
                      @endif
                    </td>
                    <td>{{date('d.m.Y H:i',strtotime($subscriber->updated_at))}}</td>
                  </tr>
              @endforeach
            @else
              <tr>
                <td colspan="5" class="text-center"><h5>Нет данных</h5></td>
              </tr>
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="card mb-4">
    <div class="card-header bg-transparent border-warning">
        <h3>Последние 10 отклонённых писем</h3>
    </div>
    <div class="row">
      <div class="col-md-12">
        <table class="table table-striped table-condensed responsive tablesorter tablesorter-default hasSaveSort" id="clicks" role="grid">
          <thead>
            <tr role="row" class="tablesorter-headerRow">
              <th>Имя</th>
              <th>Email</th>
              <th>Список</th>
              <th>Статус</th>
              <th>Дата</th>
            </tr>
          </thead>
          <tbody>
            @if(count($campaign->subscribers->where('bounced', '1')) > 0)
              @foreach($campaign->subscribers->where('bounced', '1') as $subscriber)
              <tr role="row">
                <td>{{$subscriber->name}}</td>
                <td>{{$subscriber->email}}</td>
                <td>
                  @foreach($subscriber->lists as $list)
                    <span class="badge badge-pill badge-secondary">{{$list->name }}</span>
                  @endforeach
                </td>
                <td>
                  @if($subscriber->bounced==1)
                    <span class="badge badge-dark">Отклонил</span>
                  @endif
                </td>
                <td>{{date('d.m.Y H:i',strtotime($subscriber->updated_at))}}</td>
              </tr>
              @endforeach
            @else
              <tr>
                <td colspan="5" class="text-center"><h5>Нет данных</h5></td>
              </tr>
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="card mb-4">
    <div class="card-header bg-transparent border-dark">
        <h3>Последние 10 помечены как спам</h3>
    </div>
    <div class="row">
      <div class="col-md-12">
        <table class="table table-striped table-condensed responsive tablesorter tablesorter-default hasSaveSort" id="clicks" role="grid">
          <thead>
            <tr role="row" class="tablesorter-headerRow">
              <th>Имя</th>
              <th>Email</th>
              <th>Список</th>
              <th class="text-center">Статус</th>
              <th>Дата</th>
            </tr>
          </thead>
          <tbody>
            @if(count($campaign->subscribers->where('complaint', '1')) > 0)
              @foreach($campaign->subscribers->where('complaint', '1') as $subscriber)
              <tr role="row">
                <td>{{$subscriber->name}}</td>
                <td>{{$subscriber->email}}</td>
                <td>
                  @foreach($subscriber->lists as $list)
                    <span class="badge badge-pill badge-secondary">{{$list->name }}</span>
                  @endforeach
                </td>
                <td class="text-center">
                  @if($subscriber->complaint==1)
                    <span class="badge badge-dark">Помечено как спам</span>
                  @endif
                </td>
                <td>{{date('d.m.Y H:i',strtotime($subscriber->updated_at))}}</td>
              </tr>
              @endforeach
            @else
              <tr>
                <td colspan="5" class="text-center"><h5>Нет данных</h5></td>
              </tr>
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection

@push('js')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
  <script type="text/javascript">
      $(document).ready(function() {

        var chart;
        $(document).ready(function() {

          Highcharts.setOptions({
              colors: ['#1F1F1F', '#1F1F1F', '#B94A48', '#3A87AD', '#F89406', '#468847', '#999999']
          });

            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'container',
                    type: 'bar',
                    height: 300
                },
                title: {
                    text: false
                },
                subtitle: {
                    text: false
                },
                xAxis: {
                    categories: ['Активность'],
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: false
                    }
                },
                legend: {
                  borderColor: '#E0E0E0'
              },
                tooltip: {
                    formatter: function() {
                        return ''+
                            this.series.name +': '+ this.y;
                    }
                },
                plotOptions: {
                    bar: {
                      borderWidth: 0,
                      shadow: false,
                      groupPadding: 0,
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                series: [
                {
                    name: '<?php echo _('Помечено как спам');?>',
                    data: [{{$subscriberCount['complaints']}}]
                },
                {
                    name: '<?php echo _('Отклонено');?>',
                    data: [{{$subscriberCount['bounced']}}]
                },
                {
                    name: '<?php echo _('Отписался');?>',
                    data: [{{$subscriberCount['unsubscribes']}}]
                },
                {
                    name: '<?php echo _('Клик');?>',
                    data: [{{ $campaignOpen['click'] }}]
                },
                {
                    name: '<?php echo _('Неоткрытый');?>',
                    data: [{{ $campaignOpen['not'] }}]
                },
                {
                    name: '<?php echo _('Открытый');?>',
                    data: [{{ $campaignOpen['unique'] }}]
                },
                {
                    name: '<?php echo _('Получатели');?>',
                    data: [{{ $campaign->recipients }}]
                }
                ],
                exporting: { enabled: false }
            });
        });

      });
  </script>
@endpush
