@extends('layouts.app')

@section('content')
    <form action="{{route('campaigns.update', $campaign->id)}}" method="post">
      {{ csrf_field() }}
      {{method_field('PUT')}}
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label>Выбрать шаблон</label>
            <select class="form-control" id="listselect">
              <option value="1" disabled selected>Выбрать</option>
              @foreach($templates as $template)
                <option value="{{$template->id}}">{{$template->name}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group" id="title-field" style="display: none;">
            <label for="title">Названия</label>
            <input id="title" class="form-control" type="text" name="name" value="{{$campaign->name}}">
          </div>
          <div class="form-group">
            <label>Subject</label>
            <input id="subject" class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}" type="text" name="subject" value="{{$campaign->subject}}" autofocus>
            @if ($errors->has('subject'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('subject') }}</strong>
                </span>
            @endif
            <a href="javascript:void(0);" id="set-title-btn" data-original-title="" title="">Указать названия?</a>
          </div>
          <div class="form-group">
            <label>От имени</label>
            <input class="form-control{{ $errors->has('from_name') ? ' is-invalid' : '' }}" type="text" name="from_name" value="{{$campaign->from_name}}">
            @if ($errors->has('from_name'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('from_name') }}</strong>
                </span>
            @endif
          </div>
          <div class="form-group">
            <label>От еmail</label>
            <input class="form-control{{ $errors->has('from_email') ? ' is-invalid' : '' }}" type="text" name="from_email" value="{{$campaign->from_email}}">
            @if ($errors->has('from_email'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('from_email') }}</strong>
                </span>
            @endif
          </div>
          <div class="form-group">
            <label>Ответный еmail</label>
            <input class="form-control{{ $errors->has('reply_email') ? ' is-invalid' : '' }}" type="text" name="reply_email" value="{{$campaign->reply_to}}">
            @if ($errors->has('reply_email'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('reply_email') }}</strong>
                </span>
            @endif
          </div>
          <div class="form-group">
            <label>Простой текст</label>
            <textarea class="form-control" rows="2" name="plain_text">{{$campaign->plain_text}}</textarea>
          </div>
          <div class="form-group">
            <label>
              Строка запроса
              <a class="btn btn-link" data-toggle="tooltip" data-placement="top"
                title="
                  При желании добавьте строку запроса ко всем ссылкам в бюллетене электронной почты.
                  Хорошим вариантом использования является отслеживание Google Analytics.
                  Не включать '?'
                  в строке запроса.
                "
              >
                <i class="fas fa-question-circle"></i>
              </a>
            </label>
            <input type="text" name="query_string" class="form-control" value="{{$campaign->query_string}}">
          </div>
          <!-- <div class="form-group">
            <label>Вложения</label>
            <input type="file" name="file" multiple>
            <small id="emailHelp" class="form-text text-muted">Допустимые типы файлов: jpeg, jpg, gif, png, pdf, zip</small>
          </div> -->
          <div class="row">
            <div class="col-sm-6">
              <button type="submit" name="submit" value="save" class="btn btn-success btn-sm"><i class="fas fa-check"></i> Сохранить</button>
            </div>
            <div class="col-sm-6" style="padding-left: 0px;">
              <button type="submit" name="submit" value="next" class="btn btn-primary btn-sm"><i class="fas fa-arrow-circle-right"></i> Сохранить и дальше</button>
            </div>
          </div>
        </div>
        <div class="col-md-9">
          <div class="form-group">
            <label>Контент</label>
            <textarea name="body" id="body" class="form-control" rows="5">{{$campaign->html_text}}</textarea>
          </div>
          @include('_includes.tags')
        </div>
      </div>

    </form>
@endsection

@push('js')
  <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous">
  </script>
  <script>
    ckEditor = CKEDITOR.replace( 'body', {
      fullPage: true,
      allowedContent: true,
      filebrowserUploadUrl: '{{route('file.upload',['_token' => csrf_token() ])}}',
      height: '570px',
      extraPlugins: 'codemirror,filebrowser'
    });
    ckEditor.on("instanceReady",function() {
      ckEditor.addCommand( "save", {
        modes : { wysiwyg:1, source:1 },
        exec : function () {
          var theData = ckEditor.getData();
          alert('WEEEEE')
        }
       });
    })
  </script>
    <script type="text/javascript">
      $('#listselect').change(function () {
          var listid = $(this).find('option:selected').val();
          if (listid) {
              $.ajax({
                  type: "GET",
                  url: "{{route('get.template')}}?tem_id=" + listid,
                  success: function (res) {
                      if (res) {
                        $("textarea[name=body]").empty();
                        $("textarea[name=body]").text(res);
                        CKEDITOR.instances.body.setData(res)
                      } else {
                          console.log('NO')
                          $("textarea[name=body]").empty();
                      }
                  }
              });
          } else {
              $("textarea[name=body]").empty();
          }
      });

  </script>
  <script type="text/javascript">
        $(document).ready(function() {
          $("#set-title-btn").click(function(){
              $(this).fadeOut();
              $("#set-title-btn-info").fadeOut();
              $("#title-field").slideDown("fast");
              $("#title").focus();
          });
          $("#title").blur(function(){
            if($(this).val()=='')
            {
              $("#set-title-btn").fadeIn();
              $("#set-title-btn-info").fadeIn();
              $("#title-field").slideUp("fast");
            }
          });
        });
  </script>
@endpush
