@extends('layouts.app')

@section('content')
      <div class="card">
        <div class="card-header">
            <h4>Отчеты по рассылке</h4>
        </div>
        <div class="card-body" style="padding: 10px 0 10px 0;">
            <table class="table table-hover" id="utable">
              <thead>
                  <tr>
                      <th style="width: 20px;">#</th>
                      <th>{{ trans('app.name') }}</th>
                      <th class="text-center">{{ trans('app.recipients') }}</th>
                      <th class="text-center">{{ trans('app.date.sent') }}</th>
                      <th class="text-center">{{ trans('app.uopen') }}</th>
                      <th class="text-center">{{ trans('app.uclick') }}</th>
                      <th class="text-center">{{ trans('app.action.delete') }}</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($campaigns as $k=>$campaign)
                    <tr>
                        <td>{{$k+1}}</td>
                        <td>
                          <a href="{{route('campaigns.show',$campaign->id)}}" data-toggle="tooltip" data-placement="top" title="Посмотреть отчет" >
                            <i class="fas fa-chart-line"></i> {{$campaign->name or $campaign->subject}}
                          </a>
                        </td>
                        <td class="text-center" id="progress-{{$campaign->id}}">
                            {{$campaign->recipients}}
                        </td>
                        <td class="text-center">{{empty($campaign->send_at)? '-' : date('d.m.Y H:i',strtotime($campaign->send_at))}}</td>
                        <td class="text-center">
                            <span class="badge badge-secondary">
                              {{$opens[$campaign->id]['per']}}%
                            </span> {{number_format($opens[$campaign->id]['unique'])}} открыт
                        </td>
                        <td class="text-center">
                            <span class="badge badge-secondary">{{$percentage_clicked[$campaign->id]}}%</span>
                            {{number_format(App\Campaign::get_click_percentage($campaign->id))}} клик
                        </td>
                        <td class="text-center">
                            <form action="{{action('CampaignController@destroy', $campaign->id)}}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-link"><i class="far fa-trash-alt" style="color: red;"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
              </tbody>
            </table>
        </div>

    </div>
@endsection
