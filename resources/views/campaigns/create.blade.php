@extends('layouts.app')

@section('content')
    <form action="{{route('campaigns.store')}}" method="post" id="form-campaign">
      {{ csrf_field() }}
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label>Выбрать шаблон</label>
            <select class="form-control" id="listselect">
              <option value="1" disabled selected>Выбрать</option>
              @foreach($templates as $template)
                <option value="{{$template->id}}">{{$template->name}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group" id="title-field" style="display: none;">
            <label for="title">Названия</label>
            <input id="title" class="form-control" type="text" name="name" value="{{old('name')}}">
          </div>
          <div class="form-group">
            <label>Subject</label>
            <input id="subject" class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}" type="text" name="subject" value="{{old('subject')}}" autofocus>
            @if ($errors->has('subject'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('subject') }}</strong>
                </span>
            @endif
            <a href="javascript:void(0);" id="set-title-btn" data-original-title="" title="">Указать названия?</a>
          </div>
          <div class="form-group">
            <label>От имени</label>
            <input id="from_name" class="form-control" type="text" name="from_name" value="{{$default['from_name']}}">
          </div>
          <div class="form-group">
            <label>От еmail</label>
            <input id="from_email" class="form-control" type="text" name="from_email" value="{{$default['from_email']}}">
          </div>
          <div class="form-group">
            <label>Ответный еmail</label>
            <input id="reply_email" class="form-control" type="text" name="reply_email" value="{{$default['reply_email']}}">
          </div>
          <div class="form-group">
            <label>Простой текст</label>
            <textarea class="form-control" rows="2" name="plain_text"></textarea>
          </div>
          <div class="form-group">
            <label>
              Строка запроса
              <a class="btn btn-link" data-toggle="tooltip" data-placement="top"
                title="
                  При желании добавьте строку запроса ко всем ссылкам в бюллетене электронной почты.
                  Хорошим вариантом использования является отслеживание Google Analytics.
                  Не включать '?'
                  в строке запроса."
              >
                <i class="fas fa-question-circle"></i>
              </a>
            </label>
            <input type="text" name="query" class="form-control">
          </div>
          <!-- <div class="form-group">
            <label>Вложения</label>
            <input type="file" name="file" multiple>
            <small id="emailHelp" class="form-text text-muted">Допустимые типы файлов: jpeg, jpg, gif, png, pdf, zip</small>
          </div> -->
          <div class="row">
            <div class="col-sm-6">
              <button type="submit" name="submit" value="save" class="btn btn-success btn-sm" id="save-only"><i class="fas fa-check"></i> Сохранить</button>
            </div>
            <div class="col-sm-6" style="padding-left: 0px;">
              <button type="submit" name="submit" value="next" class="btn btn-primary btn-sm"><i class="fas fa-arrow-circle-right"></i> Сохранить и дальше</button>
            </div>
          </div>
        </div>
        <div class="col-md-9">
          <div class="form-group">
            <label>Контент</label>
            <textarea name="body" id="body" class="form-control" rows="5"></textarea>
          </div>
          @include('_includes.tags')
        </div>
      </div>

    </form>
@endsection

@push('js')
  <script>
      ckEditor = CKEDITOR.replace( 'body', {
        fullPage: true,
        allowedContent: true,
        filebrowserUploadUrl: '{{route('file.upload',['_token' => csrf_token() ])}}',
        height: '570px',
        extraPlugins: 'codemirror,filebrowser'
      });
      ckEditor.on("instanceReady",function() {
        ckEditor.addCommand( "save", {
          modes : { wysiwyg:1, source:1 },
          exec : function () {
            var theData = ckEditor.getData();
            alert('WEEEEE')
          }
         });
      })
  </script>
  <!-- <script>

      $('#form-campaign').submit(function(e){
        e.preventDefault();
        let form = $(this);
        form.find('.form-control').removeClass('is-invalid');
        form.find('.invalid-feedback').detach();
        const data = $('form').serialize();
        axios.post(this.action, data)
              .then((response) => {
                swal("Успешно выполнено!", " ", "success");
                if($('#save-only').click) {
                    window.location.href = '/'
                } else {
                    window.location.href = response.data.URL;
                }
              })
              .catch((error) => {
                let errors = error.response.data.errors;
                if($.isEmptyObject(errors)===false){
                  $.each(errors, (key, value)=>{
                    $('#'+key)
                    .addClass('is-invalid')
                    .closest('.form-group')
                    .append(`<span class="invalid-feedback"><strong>${value.join(", ")}</strong></span>`)
                  })
                }
              });
      })
  </script> -->
  <script type="text/javascript">
      $('#listselect').change(function () {
          var listid = $(this).find('option:selected').val();
          if (listid) {
              $.ajax({
                  type: "GET",
                  url: "{{route('get.template')}}?tem_id=" + listid,
                  success: function (res) {
                      if (res) {
                        $("textarea[name=body]").empty();
                        $("textarea[name=body]").text(res);
                        CKEDITOR.instances.body.setData(res)
                      } else {
                          console.log('NO')
                          $("textarea[name=body]").empty();
                      }
                  }
              });
          } else {
              $("textarea[name=body]").empty();
          }
      });
  </script>
  <script type="text/javascript">
			  $(document).ready(function() {
			  	$("#set-title-btn").click(function(){
			      	$(this).fadeOut();
			      	$("#set-title-btn-info").fadeOut();
			      	$("#title-field").slideDown("fast");
			      	$("#title").focus();
			  	});
			  	$("#title").blur(function(){
				  	if($(this).val()=='')
				  	{
					  	$("#set-title-btn").fadeIn();
					  	$("#set-title-btn-info").fadeIn();
				      $("#title-field").slideUp("fast");
			      }
			  	});
			  });
  </script>
@endpush
