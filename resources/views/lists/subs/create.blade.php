@extends('layouts.app')

@section('content')
<div class="card">
  <div class="card-header">
    Добавить подписчика в список {{$list->name}}
  </div>
  <div class="card-body p-0">
    <table class="table table-hover" >
      <thead>
        <tr>
          <th scope="col">#</th>
          <th class="text-center">Имя</th>
          <th class="text-center">Email</th>
          <th class="text-center">Список</th>
          <th style="width: 40px">Действие</th>
        </tr>
      </thead>
      <tbody>
        @if(count($subscribers) > 0)
          @foreach($subscribers as $key=>$subscriber)
            <tr>
              <td>{{$key+1}}</td>
              <td class="text-center">{{$subscriber->name}}</td>
              <td class="text-center">{{$subscriber->email}}</td>
              <td class="text-center">
                @foreach($subscriber->lists as $li)
                    <span class="badge badge-pill badge-info">{{$li->name}}</span>
                @endforeach
              </td>
              <td>
                  <a href="{{route('subs.attach',[$list->id, $subscriber->id])}}" class="btn btn-secondary">Добавить в список</a>
              </td>
            </tr>
          @endforeach
        @else
          <tr>
            <td colspan="5" class="text-center"><h5>Все существующие подписчики добавлены</h5></td>
          </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>

@stop
