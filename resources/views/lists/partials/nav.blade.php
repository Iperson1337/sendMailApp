<ul class="nav nav-tabs border-bottom-0">
    <li class="nav-item">
        <a class="nav-link {{$active === 'all' ? 'active' : ''}}" href="{{route('lists.show',$list->id)}}">
          Все
          <span class="badge badge-pill badge-primary">
            {{$list->subscribers->count()}}
          </span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{$active === 'active' ? 'active' : ''}}" href="{{route('lists.show',$list->id)}}?status=active">
          Активный
          <span class="badge badge-pill badge-success">
            {{$list->subscribers->where('unsubscribed', '0')->where('bounced', '0')->where('complaint', '0')->where('confirmed', '1')->count()}}
          </span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{$active === 'unconfirmed' ? 'active' : ''}}" href="{{route('lists.show',$list->id)}}?status=unconfirmed">
          Неподтвержденный
          <span class="badge badge-pill badge-secondary">
            {{$list->subscribers->where('confirmed', 0)->count()}}
          </span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{$active === 'unsubscribed' ? 'active' : ''}}" href="{{route('lists.show',$list->id)}}?status=unsubscribed">
          Отписался
          <span class="badge badge-pill badge-danger">
            {{$list->subscribers->where('unsubscribed', 1)->count()}}
          </span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{$active === 'bounced' ? 'active' : ''}}" href="{{route('lists.show',$list->id)}}?status=bounced">
          Отскочила
          <span class="badge badge-pill badge-dark">
            {{$list->subscribers->where('bounced', 1)->count()}}
          </span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{$active === 'complaint' ? 'active' : ''}}" href="{{route('lists.show',$list->id)}}?status=complaint">
          Отмечено как спам
          <span class="badge badge-pill badge-dark">
            {{$list->subscribers->where('complaint', 1)->count()}}
          </span>
        </a>
    </li>
</ul>
