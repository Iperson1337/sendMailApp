@extends('layouts.app')

@section('content')

  <form method="POST" action="{{route('mass.action', $list->id)}}">
    {{ csrf_field() }}
    <h2 class="mb-3">Списки подписчиков</h2>
    <button class="btn btn-dark btn-sm" type="button" onclick="window.location='{{route('subs.add', $list->id)}}'"><i class="fas fa-plus"></i> Добавить подписчика в список</button>
    <button class="btn btn-dark btn-sm" type="submit" name="button" value="unsubscribe"><i class="fas fa-ban"></i> Отписка отмеченных</button>
    <button class="btn btn-dark btn-sm" type="submit" name="button" value="detach"><i class="far fa-trash-alt"></i> Убрать отмеченные из списка</button>
    <div class="card mt-3">
      <div class="card-body">
        Список: <span class="badge badge-primary">{{$list->name}}</span> | <a href="{{route('lists.index')}}" class="text-dark">Вернуться назад</a>
        <a href="{{route('lists.edit',$list->id)}}" class="ml-1 text-dark float-right"><i class="fas fa-wrench"></i> Настройки списка</a>
      </div>
    </div>
    <div class="row mb-3">
      <div class="col-md-12">
        <div id="container" style="min-height:200px;margin-top:20px;"></div>
      </div>
    </div>
    <div class="card" style="border: 0;">
      <div class="card-header pb-0 pl-0" style="background-color: #f9f9f9;">
        @include('lists.partials.nav', ['list' => $list, 'active' => $active])
      </div>
      <div class="card-body border-top-0" style="border: 1px solid rgba(0,0,0,.125); border-radius: 0px 0px 0.25rem 0.25rem; padding: 0;">
        <table class="table table-striped table-condensed responsive">
            <thead>
              <tr>
                <th></th>
                <th>Имя</th>
                <th>Email</th>
                <th class="text-center">Последняя активность</th>
                <th class="text-center" style="width: 20px;">Статус</th>
                <th class="text-center" style="width: 20px;">Отписаться</th>
                <th class="text-center" style="width: 20px;">Убрать</th>
              </tr>
            </thead>
            <tbody>
              @forelse($subscribers as $subscriber)
                <tr>
                  <td><input type="checkbox" name="subscriber[]" value="{{$subscriber->id}}"></td>
                  <td>{{$subscriber->name}}</td>
                  <td>{{$subscriber->email}}</td>
                  <td class="text-center">{{date('d.m.Y H:i', strtotime($subscriber->updated_at))}}</td>
                  <td>
                    @if($subscriber->bounced === 1)
                      <span class="badge badge-dark">Отскочила</span>
                    @elseif($subscriber->complaint === 1)
                      <span class="badge badge-dark">Отмечено как спам</span>
                    @elseif($subscriber->unsubscribed === 1)
                      <span class="badge badge-danger">Отписался</span>
                    @else
                      <span class="badge badge-success">Подписан</span>
                    @endif
                  </td>
                  <td class="text-center">
                    @if($subscriber->bounced === 1 || $subscriber->complaint === 1)
                      -
                    @else
                      @if($subscriber->unsubscribed === 0 || is_null($subscriber->unsubscribed))
                        <a href="{{route('subs.unsubscribe', $subscriber->id)}}" style="color: red">
                          <i class="fas fa-ban" data-toggle="tooltip" data-placement="top" title="отписаться"></i>
                        </a>
                      @else
                        <a href="{{route('subs.subscribe', $subscriber->id)}}">
                          <i class="fas fa-check" data-toggle="tooltip" data-placement="top" title="подписка"></i>
                        </a>
                      @endif
                    @endif
                  </td>
                  <td class="text-center">
                    <a href="{{route('subs.detach', [$list->id, $subscriber->id])}}">
                      <i class="far fa-trash-alt" data-toggle="tooltip" data-placement="top" title="Убрать из списка"></i>
                    </a>
                  </td>
                </tr>
              @empty
                <tr>
                  <td colspan="7" class="text-center"><h5>Нет данных</h5></td>
                </tr>
              @endforelse
            </tbody>
        </table>
      </div>
    </div>
  </form>

@stop
@push('js')
  @if(Session::has('errors'))
      <script>
          swal({
            title: "Ошибка!",
            text: "@foreach($errors->all() as $error) {{ $error }} @endforeach",
            icon: "error",
          })
      </script>
  @endif
  <script src="{{asset('js/highcharts/highcharts.js')}}"></script>
  <script type="text/javascript">
    var month = new Array();
      month[0]="Январь";
      month[1]="Фебраль";
      month[2]="Март";
      month[3]="Апрель";
      month[4]="Май";
      month[5]="Июнь";
      month[6]="Июль";
      month[7]="Август";
      month[8]="Сеньтябрь";
      month[9]="Октябрь";
      month[10]="Ноябьр";
      month[11]="Декабрь";
    var chart;
    $(document).ready(function() {
        Highcharts.setOptions({
            colors: ['#468847', '#B94A48', '#333333']
        });
        chart = new Highcharts.Chart({
            chart: {
              renderTo: 'container',
              type: 'areaspline',
              marginBottom: 25
            },
            title: {
              text: false
            },
            subtitle: {
              text: false
            },
            xAxis: {
              categories: [
                @php
                  $month_array = array();
                  $year_array = array();
                  $subs = $list->subscribers->where('unsubscribed', '0')
                                ->where('bounced', '0')
                                ->where('complaint', '0')
                                ->where('confirmed', '1')
                                ->last();
                  if(empty($subs->created_at)){
                    $month_max = time();
                  }else{
                    $month_max = strtotime($subs->created_at);
                  }

                  $month = strftime('%m', $month_max)-1;
                  $year = strftime('%y', $month_max);
                  for($i=0;$i<12;$i++)
                  {
                    array_push($month_array, $month);
                    array_push($year_array, $year);
                    $month--;
                    if($month<0)
                    {
                      $month = 11;
                      $year--;
                    }
                  }

                  $month_array = array_reverse($month_array);
                  $year_array = array_reverse($year_array);

                  for($i=0;$i<12;$i++)
                  {
                    echo 'month['.$month_array[$i].']'.'+" '.$year_array[$i].'"';
                    if($i<11)
                      echo ',';
                  }
                @endphp
              ]
            },
            yAxis: {
              title: {
                text: false
              },
              plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
              }]
            },
            plotOptions: {
              line: {
                stacking: 'normal'
              },
              series: {
                        marker: {
                            enabled: false
                        }
                    }
            },
            tooltip: {
              formatter: function() {
                  return '<b>'+ this.series.name +'</b><br/>'+
                  this.x +': '+ this.y;
              }
            },
            legend: {
              enabled: false
            },
            credits: {
                      enabled: false
                  },
            series: [{
                      name: 'Подписчики',
                      data: [
                        @php
                          $graph_array = array();
                          $onemonth = 2629746;
                          $maxmonth = $month_max;
                          for($i=0;$i<12;$i++)
                          {
                            $subscribers = $list->subscribers->where('created_at', '<=', date('Y-m-d H:i:s', $maxmonth))
                                          ->where('unsubscribed', '0')
                                          ->where('bounced', '0')
                                          ->where('complaint', '0')
                                          ->where('confirmed', '1')
                                          ->count();
                            if ($subscribers > 0)
                            {
                                array_push($graph_array, $subscribers);
                            }
                            else
                              array_push($graph_array, '0');

                            $maxmonth = $maxmonth - $onemonth;
                          }

                          $graph_array = array_reverse($graph_array);

                          for($i=0;$i<12;$i++)
                          {
                            echo $graph_array[$i];

                            if($i<11)
                            echo ',';
                          }

                        @endphp
                      ]
                  }]
        });
    });
  </script>
@endpush
