@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        Редактирования списка {{$list->name}}
    </div>
    <div class="card-body">
        <form action="{{route('lists.update', $list->id)}}" method="post">
          {{ csrf_field() }}
          {{method_field('PUT')}}
          <div class="form-group">
            <label>Названия</label>
            <input type="text" name="name" value="{{$list->name}}" class="form-control" required>
          </div>
          <button type="submit" name="button" class="btn btn-dark"><i class="fas fa-plus"></i> Изменить</button>
        </form>
    </div>
</div>
@stop
