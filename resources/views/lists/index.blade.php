@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <a href="{{route('lists.create')}}" class="btn btn-primary">
                <i class="fas fa-plus"></i> Создать список
            </a>
        </div>
        <div class="card-body" style="padding: 10px 0 10px 0;">
            <table class="table table-hover" id="utable">
            <thead>
                <tr>
                    <th style="width: 20px;">#</th>
                    <th>Названия</th>
                    <th style="width: 20px;" class="text-center">{{ trans('app.active') }}</th>
                    <th class="text-center">{{ trans('app.unsubscribers') }}</th>
                    <th class="text-center">{{ trans('app.bounced') }}</th>
                    <th style="width: 20px;" class="text-center">{{ trans('app.action.edit') }}</th>
                    <th style="width: 20px;" class="text-center">{{ trans('app.action.delete') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($lists as $k=>$list)
                    <tr>
                        <td>{{$k+1}}</td>
                        <td><a href="{{route('lists.show',$list->id)}}">{{$list->name}}</a></td>
                        <td class="text-center">
                          {{$active[$list->id]}}
                        </td>
                        <td class="text-center">
                          <span class="badge badge-secondary">
                            {{$total[$list->id] == 0 ? round($deactive[$list->id] * 100, 2) : round($deactive[$list->id] / ($total[$list->id]) * 100, 2)}}%
                          </span>
                          {{$deactive[$list->id]}} подписчик
                        </td>
                        <td class="text-center">
                          <span class="badge badge-secondary">
                            {{$total[$list->id] == 0 ? round($bounced[$list->id] * 100, 2) : round($bounced[$list->id] / ($total[$list->id]) * 100, 2)}}%
                          </span> {{$bounced[$list->id]}} подписчик
                        </td>
                        <td class="text-center"><a href="{{route('lists.edit',$list->id)}}"><i class="fas fa-edit"></i></a></td>
                        <td class="text-center">
                            <form action="{{action('ListController@destroy', $list->id)}}" method="post">
                                {{ csrf_field() }}
                                {{method_field('DELETE')}}
                                <button type="submit" class="btn btn-link"><i class="far fa-trash-alt" style="color: red;"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>

    </div>
@stop
