@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        {{ trans('app.action.create') }} список
    </div>
    <div class="card-body">
        <form action="{{route('lists.store')}}" method="post">
          {{ csrf_field() }}
          <div class="form-group">
            <label>{{ trans('app.name') }}</label>
            <input type="text" name="name" value="" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" required>
            @if ($errors->has('name'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
          </div>
          <button type="submit" name="button" class="btn btn-dark"><i class="fas fa-plus"></i> Добавить</button>
        </form>
    </div>
</div>
@stop
