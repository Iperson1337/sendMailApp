@extends('layouts.app')

@section('content')
    <form action="{{route('templates.update', $template->id)}}" method="post">
    	{{ csrf_field() }}
        {{method_field('PUT')}}
    	<div class="row">
    		<div class="col-md-3">
    			<div class="form-group">
				    <label for="exampleInputEmail1">Названия шаблона</label>
				    <input type="text" class="form-control" id="exampleInputEmail1" name="name" placeholder="Названия этого шаблона" value="{{$template->name}}">
				  </div>
				  <button type="submit" class="btn btn-primary">Сохранить</button>
    		</div>
    		<div class="col-md-9">
    			<div class="form-group">
				    <label for="exampleInputEmail1">Тела шаблона</label>
				    <textarea name="body" id="body" class="form-control" rows="3">{!!$template->body!!}</textarea>
				  </div>

    		</div>
    	</div>

    </form>
@endsection

@push('js')
	<script>
    ckEditor = CKEDITOR.replace( 'body', {
      fullPage: true,
      allowedContent: true,
      filebrowserUploadUrl: '{{route('file.upload',['_token' => csrf_token() ])}}',
      height: '570px',
      extraPlugins: 'codemirror,filebrowser'
    });
    ckEditor.on("instanceReady",function() {
      ckEditor.addCommand( "save", {
        modes : { wysiwyg:1, source:1 },
        exec : function () {
          var theData = ckEditor.getData();
          alert('WEEEEE')
        }
       });
    })
	</script>
@endpush
