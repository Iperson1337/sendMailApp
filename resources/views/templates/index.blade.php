@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <a href="{{route('templates.create')}}" class="btn btn-primary">
                <i class="fas fa-plus"></i> Создать шаблон
            </a>
        </div>
        <div class="card-body" style="padding: 0;">
            <table class="table table-hover" id="users-table">
            <thead>
                <tr>
                    <th style="width: 20px;">#</th>
                    <th>Названия</th>
                    <th style="width: 20px;" class="text-center">Просмотр</th>
                    <th style="width: 20px;" class="text-center">Редактировать</th>
                    <th style="width: 20px;" class="text-center">Удалить</th>
                </tr>
            </thead>
            <tbody>
                @foreach($templates as $k=>$template)
                    <tr>
                        <td>{{$k+1}}</td>
                        <td><a href="{{route('templates.edit',$template->id)}}">{{$template->name}}</a></td>
                        <td class="text-center"><a href="{{route('templates.show',$template->id)}}"><i class="fas fa-eye"></i></a></td>
                        <td class="text-center"><a href="{{route('templates.edit',$template->id)}}"><i class="fas fa-edit"></i></a></td>
                        <td class="text-center">
                            <form action="{{action('TemplateController@destroy', $template->id)}}" method="post">
                                {{ csrf_field() }}
                                {{method_field('DELETE')}}
                                <button type="submit" class="btn btn-link"><i class="far fa-trash-alt" style="color: red;"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>

    </div>
@stop
