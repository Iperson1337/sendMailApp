@extends('layouts.app')

@section('content')
    <form action="{{route('templates.store')}}" method="post">
    	{{ csrf_field() }}
    	<div class="row">
    		<div class="col-md-3">
    			<div class="form-group">
				    <label for="exampleInputEmail1">Названия шаблона</label>
				    <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" name="name" placeholder="Названия этого шаблона" autofocus>
            @if ($errors->has('name'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
				  </div>
				  <button type="submit" class="btn btn-primary"><i class="fas fa-check"></i> Сохранить</button>
    		</div>
    		<div class="col-md-9">
    			<div class="form-group">
				    <label for="exampleInputEmail1">Тела шаблона</label>
				    <textarea name="body" id="body" class="form-control{{ $errors->has('body') ? ' is-invalid' : '' }}" rows="3">{{old('body')}}</textarea>
            @if ($errors->has('body'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('body') }}</strong>
                </span>
            @endif
				  </div>

    		</div>
    	</div>

    </form>
@endsection

@push('js')
	<script>

    ckEditor = CKEDITOR.replace( 'body', {
      fullPage: true,
      allowedContent: true,
      filebrowserUploadUrl: '{{route('file.upload',['_token' => csrf_token() ])}}',
      height: '570px',
      extraPlugins: 'codemirror,filebrowser'
    });

    ckEditor.on("instanceReady",function() {
      ckEditor.addCommand( "save", {
        modes : { wysiwyg:1, source:1 },
        exec : function () {
          var theData = ckEditor.getData();
          if($('#name').val()){
            var name = $('#name').val()
          }else{
            var name = 'tem'
          }

          $.ajax({
             url: "{{route('templates.store')}}",
             type: 'POST',
             data: {_token: '{{csrf_token()}}', body: theData, name: name, ajax: true},
             dataType: 'json',
             success: function(data) {
                if(data.saved){
                  swal('Успешно выполнено!', data.message, 'success');
                }
                window.location = "{{route('templates.index')}}"
             },
             error: function(err){
               swal('Упсс...', 'Что-то пошло не так с ajax!', 'error');
             }
          });
        }
       });
    })
	</script>
@endpush
