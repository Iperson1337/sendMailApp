<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    // Campaign status
    const STATUS_DRAFT = 'draft';
    const STATUS_SENDING = 'sending';
    const STATUS_DONE = 'done';
    const STATUS_ERROR = 'error';
    const STATUS_QUEUE = 'queue';

    protected $fillable = [
      'name',
      'subject',
      'from_name',
      'from_email',
      'reply_to',
      'plain_text',
      'html_text',
      'query_string',
      'to_send',
      'status',
      'recipients',
      'timeout_check',
      'opens',
      'timezone',
      'errors',
      'run_date',
      'send_at',
      'complaint_setup',
      'bounce_setup'
    ];

    protected $dates = ['created_at', 'updated_at', 'run_date'];

    public function sending()
    {
        $this->status = self::STATUS_SENDING;
        $this->save();
    }

    public function draft()
    {
        $this->status = self::STATUS_DRAFT;
        $this->save();
    }

    public function done()
    {
        $this->status = self::STATUS_DONE;
        $this->send_at = \Carbon\Carbon::now();
        $this->save();
    }

    public function queue($datatime)
    {
        $this->status = self::STATUS_QUEUE;
        $this->run_date = $datatime;
        $this->save();
    }

    public function isSending(): bool
    {
        return $this->status === self::STATUS_SENDING;
    }

    public function isDraft(): bool
    {
        return $this->status === self::STATUS_DRAFT;
    }

    public function isDone(): bool
    {
        return $this->status === self::STATUS_DONE;
    }

    public function isQueue(): bool
    {
        return $this->status === self::STATUS_QUEUE;
    }

    public static function get_click_percentage($cid)
    {
      $campaign = self::findOrFail($cid);
      $clicks_join = '';
      $clicks_array = array();
      $clicks_unique = 0;

      foreach ($campaign->links as $link) {
        $clicks = $link->clicks;
        if($clicks!=''){
          $clicks_join .= $clicks.',';
        }
      }
      $clicks_array = explode(',', $clicks_join);
      $clicks_unique = count(array_unique($clicks_array));

      return $clicks_unique-1;
    }

    public function links()
    {
        return $this->belongsToMany('App\Link', 'campaign_links');
    }

    public function lists()
    {
        return $this->belongsToMany('App\SubscriberList', 'campaign_lists', 'campaign_id', 'list_id');
    }

    public function subscribers()
    {
        return $this->hasMany('App\Subscriber', 'last_campaign');
    }
}
