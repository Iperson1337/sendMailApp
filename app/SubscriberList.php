<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriberList extends Model
{
  protected $table='lists';
  protected $fillable = [
    'name',
    'description'
  ];

  public function campaigns()
  {
      return $this->belongsToMany('App\Campaign', 'campaign_lists', 'list_id', 'campaign_id');
  }

  public function subscribers()
  {
      return $this->belongsToMany('App\Subscriber', 'subscriber_lists', 'list_id', 'subscriber_id');
  }
}
