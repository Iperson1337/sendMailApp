<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{

  protected $table = 'subscribers';
  protected $fillable = [
    'email',
    'name',
    'custom_fields',
    'bounced',
    'bounce_soft',
    'unsubscribe',
    'complaint',
    'last_campaign',
    'last_ares',
    'confirmed',
  ];

  public function scopeSendCampaign($query) {
    return $query->where('unsubscribed', '0')
                ->where('bounced', '0')
                ->where('complaint', '0')
                ->where('confirmed', '1');
  }

  public static function get_bounced_count($cid,$soft='')
  {
      if($soft=='soft') $subscribers = self::where('last_campaign', $cid)->where('bounce_soft', 1)->get();
      else $subscribers = self::where('last_campaign', $cid)->where('bounced', 1)->get();
      if (count($subscribers) > 0) {
          return count($subscribers);
      }
      else {
        return 0;
      }
  }

  public static function get_complaints_count($cid)
  {
    $subscribers = self::where('last_campaign', $cid)->where('complaint', 1)->get();
    if ($subscribers->count() > 0)
        return $subscribers->count();
    else
      return 0;
  }

  public static function get_unsubscribes_count($cid)
  {
      $subscribers = self::where('last_campaign', $cid)->where('unsubscribed', 1)->get();
      if ($subscribers->count() > 0)
          return $subscribers->count();
      else
          return 0;
  }



  public function lists()
  {
      return $this->belongsToMany('App\SubscriberList', 'subscriber_lists', 'subscriber_id', 'list_id');
  }
}
