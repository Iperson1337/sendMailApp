<?php

namespace App\Mail;

use Helper;
use App\Campaign;
use App\Subscriber;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendCampaign extends Mailable
{
    use Queueable, SerializesModels;

    public $campaign;
    public $alteredCampaign;
    public $subscriber;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Campaign $campaign, Subscriber $subscriber)
    {
        $this->campaign = $campaign;
        $this->subscriber = $subscriber;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->alteredCampaign = Helper::injectUrl($this->campaign->id, $this->subscriber->id);
        return $this->from($this->alteredCampaign->from_email, $this->alteredCampaign->from_name)
                    ->subject($this->alteredCampaign->subject)
                    ->view('mail.html')
                    ->text('mail.text')
                    ->with([
                      'content'=> $this->alteredCampaign
                    ]);
    }
}
