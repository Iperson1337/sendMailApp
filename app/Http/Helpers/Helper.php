<?php

namespace App\Http\Helpers;
use App\Subscriber;
use App\Campaign;
use App\Link;

class Helper {

    public static function delete_between($beginning, $end, $string)
    {
      $beginningPos = strpos($string, $beginning);
      $endPos = strpos($string, $end);
      if ($beginningPos === false || $endPos === false) return $string;
      $textToDelete = substr($string, $beginningPos, ($endPos + strlen($end)) - $beginningPos);
      return str_replace($textToDelete, "", $string);
    }

    public static function parseUrl($id)
    {
        $campaign = Campaign::findOrFail($id);
        $html = $campaign->html_text;
        $links = array();
        //extract all links from HTML
        preg_match_all('/href=["\']([^"\']+)["\']/i', $html, $matches, PREG_PATTERN_ORDER);
        $matches = array_unique($matches[1]);
        foreach($matches as $var) {
          $var = $campaign->query_string!='' ? ((strpos($var,'?') !== false) ? $var.'&'.$campaign->query_string : $var.'?'.$campaign->query_string) : $var;
          if(substr($var, 0, 1)!="#" && substr($var, 0, 6)!="mailto" && substr($var, 0, 3)!="tel" && substr($var, 0, 3)!="sms" && substr($var, 0, 13)!="[unsubscribe]" && substr($var, 0, 12)!="[webversion]" && !strpos($var, 'fonts.googleapis.com'))
          {
              array_push($links, $var);
          }
        }

        for($i=0;$i<count($links);$i++) {
            $link = new Link;
            $link->link = $links[$i];
            $link->save();
            $campaign->links()->attach($link->id);
        }
    }//end ParseUrl

    public static function injectUrl($cid, $sid)
    {
        $campaignPaste = Campaign::findOrFail($cid);
        $subscriber = Subscriber::findOrFail($sid);
        $subID = encrypt($sid);
        $camID = encrypt($campaignPaste->id);
        $subsEmail = encrypt($subscriber->email);

        if (count($campaignPaste->links) > 0)
        {
            foreach($campaignPaste->links as $link)
            {
              $linkID = encrypt($link->id);
              if($campaignPaste->query_string!='')
              {
                $link = (strpos($link->link,'?'.$campaignPaste->query_string) !== false) ? str_replace('?'.$campaignPaste->query_string, '', $link->link) : str_replace('&'.$campaignPaste->query_string, '', $link->link);
              }
              else
              {
                $link = $link->link;
              }
              //replace new links on HTML code
              $campaignPaste->html_text = str_replace('href="'.$link.'"', 'href="'.route('link.click', [$subID, $linkID, $camID]).'"', $campaignPaste->html_text);
              $campaignPaste->html_text = str_replace('href=\''.$link.'\'', 'href="'.route('link.click', [$subID, $linkID, $camID]).'"', $campaignPaste->html_text);

              //replace new links on Plain Text code
              $campaignPaste->plain_text = str_replace($link, route('link.click', [$subID, $linkID, $camID]), $campaignPaste->plain_text);
            }
        }

        $campaignPaste->subject = self::tagTreated($campaignPaste->subject, $subscriber);
        $campaignPaste->html_text = self::tagTreated($campaignPaste->html_text, $subscriber);
        $campaignPaste->plain_text = self::tagTreated($campaignPaste->plain_text, $subscriber);

        $link_unsubscribe = route('unsubscribe',[$subsEmail, $camID]);
        $image_link = route('open.tracker',[$camID, $subID]);

        //unsubscribe tag and Link
        $campaignPaste->html_text = str_replace('<unsubscribe', '<a href="'.$link_unsubscribe.'"', $campaignPaste->html_text);
        $campaignPaste->html_text = str_replace('</unsubscribe>', '</a>', $campaignPaste->html_text);
        $campaignPaste->html_text = str_replace('[unsubscribe]', $link_unsubscribe, $campaignPaste->html_text);
        $campaignPaste->plain_text = str_replace('[unsubscribe]', $link_unsubscribe, $campaignPaste->plain_text);
        //Email tag
        $campaignPaste->html_text = str_replace('[Email]', $subscriber->email, $campaignPaste->html_text);
        $campaignPaste->plain_text = str_replace('[Email]', $subscriber->email, $campaignPaste->plain_text);
        $campaignPaste->subject = str_replace('[Email]', $subscriber->email, $campaignPaste->subject);

        //add tracking 1 by 1px image
        $campaignPaste->html_text .= '<img src="'.$image_link.'" alt="" style="width:1px;height:1px;"/>';

        return $campaignPaste;
    }

    public static function tagTreated($treated, $subscriber)
    {
        preg_match_all('/\[([a-zA-Z0-9!#%^&*()+=$@._\-\:|\/?<>~`"\'\s]+),\s*fallback=/i', $treated, $matches_var, PREG_PATTERN_ORDER);
        preg_match_all('/,\s*fallback=([a-zA-Z0-9!,#%^&*()+=$@._\-\:|\/?<>~`"\'\s]*)\]/i', $treated, $matches_val, PREG_PATTERN_ORDER);
        preg_match_all('/(\[[a-zA-Z0-9!#%^&*()+=$@._\-\:|\/?<>~`"\'\s]+,\s*fallback=[a-zA-Z0-9!,#%^&*()+=$@._\-\:|\/?<>~`"\'\s]*\])/i', $treated, $matches_all, PREG_PATTERN_ORDER);
        preg_match_all('/\[([^\]]+),\s*fallback=/i', $treated, $matches_var, PREG_PATTERN_ORDER);
        preg_match_all('/,\s*fallback=([^\]]*)\]/i', $treated, $matches_val, PREG_PATTERN_ORDER);
        preg_match_all('/(\[[^\]]+,\s*fallback=[^\]]*\])/i', $treated, $matches_all, PREG_PATTERN_ORDER);
        $matches_var = $matches_var[1];
        $matches_val = $matches_val[1];
        $matches_all = $matches_all[1];
        for($i=0;$i<count($matches_var);$i++)
        {
          $field = $matches_var[$i];
          $fallback = $matches_val[$i];
          $tag = $matches_all[$i];
          //if tag is Name
          if($field=='Name')
          {
            if(empty($subscriber->name))
              $treated = str_replace($tag, $fallback, $treated);
            else
              $treated = str_replace($tag, $subscriber[strtolower($field)], $treated);
          }
          else //if not 'Name', it's a custom field
          {
            //if subscriber has no custom fields, use fallback
            if(empty($subscriber->custom_fields))
              $treated = str_replace($tag, $fallback, $treated);
            //otherwise, replace custom field tag
            else
            {
                $subsList = $subscriber->lists->last();
                $list = SubscriberList::select('custom_fields')->where('id', $subsList->id)->get();
                $custom_fields = $list->custom_fields;

                $custom_fields_array = explode('%s%', $custom_fields);
                $custom_values_array = explode('%s%', $custom_values);
                $cf_count = count($custom_fields_array);
                $k = 0;

                for($j=0;$j<$cf_count;$j++)
                {
                  $cf_array = explode(':', $custom_fields_array[$j]);
                  $key = str_replace(' ', '', $cf_array[0]);

                    //if tag matches a custom field
                  if($field==$key)
                  {
                      //if custom field is empty, use fallback
                    if($custom_values_array[$j]=='')
                      $treated = str_replace($tag, $fallback, $treated);
                      //otherwise, use the custom field value
                    else
                    {
                        //if custom field is of 'Date' type, format the date
                      if($cf_array[1]=='Date')
                        $treated = str_replace($tag, strftime("%a, %b %d, %Y", $custom_values_array[$j]), $treated);
                        //otherwise just replace tag with custom field value
                      else
                        $treated = str_replace($tag, $custom_values_array[$j], $treated);
                    }
                  }
                  else
                    $k++;
                }
                if($k==$cf_count)
                  $treated = str_replace($tag, $fallback, $treated);
            }
          }
        }
        return $treated;
    }

    public static function amozomSNS()
    {
      $aws_endpoint_array = explode('.', get_app_info('ses_endpoint'));
          $aws_endpoint = $aws_endpoint_array[1];
          $sns = new AmazonSNS(get_app_info('s3_key'), get_app_info('s3_secret'), $aws_endpoint);
          $bounces_topic_arn = '';
          $bounces_subscription_arn = '';
          $complaints_topic_arn = '';
          $complaints_subscription_arn = '';
          //Get protocol of endpoint
            $protocol_array = explode(':', get_app_info('path'));
            $protocol = $protocol_array[0];
          try
          {
            //Get list of SNS topics and subscriptions
            $v_subscriptions = $sns->ListSubscriptions();
            foreach ($v_subscriptions as $subscription)
            {
              $TopicArn = $subscription['TopicArn'];
              $Endpoint = $subscription['Endpoint'];
              if($Endpoint==route('bounces'))
              {
                $bounces_topic_arn = $TopicArn;
                $bounces_subscription_arn = $Endpoint;
              }
              if($Endpoint==route('complaints'))
              {
                $complaints_topic_arn = $TopicArn;
                $complaints_subscription_arn = $Endpoint;
              }
            }

            //Create 'bounces' SNS topic
            try {
              $bounces_topic_arn = $sns->CreateTopic('bounces');
            }
            catch (SNSException $e) {
              return '<p class="error">'._('Error').' ($sns->CreateTopic(\'bounces\')): '.$e->getMessage().'. '._('Please try again by refreshing this page. If this error persist, visit your Amazon SNS console and delete all \'Topics\' and \'Subscriptions\' and try again.')."<br/><br/></p>";
            }

            //Create 'complaints' SNS topic
            try {
              $complaints_topic_arn = $sns->CreateTopic('complaints');
            }
            catch (SNSException $e) {
              return '<p class="error">'._('Error').' ($sns->CreateTopic(\'complaints\')): '.$e->getMessage().'. '._('Please try again by refreshing this page. If this error persist, visit your Amazon SNS console and delete all \'Topics\' and \'Subscriptions\' and try again.')."<br/><br/></p>";}

              //If 'bounces' and 'complaints' SNS topics exists, create SNS subscriptions for them
              if($bounces_topic_arn!='' && $complaints_topic_arn!='')
              {
                //Create 'bounces' SNS subscription
                try {$bounces_subscribe_endpoint = $sns->Subscribe($bounces_topic_arn, $protocol, get_app_info('path').'/includes/campaigns/bounces.php');}
                catch (SNSException $e) {
                  echo '<p class="error">'._('Error').' ($sns->Subscribe(\'bounces\')): '.$e->getMessage().'. '._('Please try again by refreshing this page. If this error persist, visit your Amazon SNS console and delete all \'Topics\' and \'Subscriptions\' and try again.')."<br/><br/></p>";
                }

                //Create 'complaints' SNS subscription
                try {
                  $complaints_subscribe_endpoint = $sns->Subscribe($complaints_topic_arn, $protocol, get_app_info('path').'/includes/campaigns/complaints.php');
                }
                catch (SNSException $e) {
                  return '<p class="error">'._('Error').' ($sns->Subscribe(\'complaints\')): '.$e->getMessage().'. '._('Please try again by refreshing this page. If this error persist, visit your Amazon SNS console and delete all \'Topics\' and \'Subscriptions\' and try again.')."<br/><br/></p>";
                }
              }
              else return

               '<p class="error">'._('Error: Unable to create bounces and complaints SNS topics, please try again by refreshing this page.')."<br/><br/></p>";

              //Set SNS 'Notifications' for 'From email'
                require_once('includes/helpers/ses.php');
            $ses = new SimpleEmailService(get_app_info('s3_key'), get_app_info('s3_secret'), get_app_info('ses_endpoint'));

            //Set 'bounces' Notification
            $ses->SetIdentityNotificationTopic($from_email,$bounces_topic_arn,'Bounce');
            $ses->SetIdentityNotificationTopic($from_email_domain,$bounces_topic_arn,'Bounce');

            //Set 'complaints' Notification
            $ses->SetIdentityNotificationTopic($from_email,$complaints_topic_arn,'Complaint');
            $ses->SetIdentityNotificationTopic($from_email_domain,$complaints_topic_arn,'Complaint');

            //Disable email feedback forwarding
            $ses->setIdentityFeedbackForwardingEnabled($from_email, 'false');
            $ses->setIdentityFeedbackForwardingEnabled($from_email_domain, 'false');

          }
          catch (SNSException $e){

          }
    }
}
