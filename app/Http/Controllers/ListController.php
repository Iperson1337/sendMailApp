<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ListRequest;
use App\SubscriberList;
use App\Subscriber;
use Illuminate\Support\Facades\Storage;

class ListController extends Controller
{

    public function index()
    {
      $lists = SubscriberList::with('subscribers')->get();
      $active = array();
      $deactive = array();
      foreach ($lists as $key => $list) {
        $total[$list->id] = $list->subscribers->count();
        $active[$list->id] = $list->subscribers->where('unsubscribed', '0')
                          ->where('bounced', '0')
                          ->where('complaint', '0')
                          ->where('confirmed', '1')
                          ->count();
        $deactive[$list->id] = $list->subscribers->where('unsubscribed', '1')->count();
        $bounced[$list->id] = $list->subscribers->where('bounced', '1')->count();
      }

      return view('lists.index',compact('lists','active','deactive','bounced','total'));
    }

    public function create()
    {
        return view('lists.create');
    }

    public function store(ListRequest $request)
    {
        $lists = new SubscriberList;
        $lists->name = $request->name;
        $lists->save();
        return redirect()->action('ListController@index')->with('status', 'Список успешно добавлен');
    }

    public function show(Request $request, $id)
    {
      $list = SubscriberList::findOrFail($id);

      if($request->status === 'active') {
          $active = 'active';
          $subscribers = $list->subscribers->where('unsubscribed', '0')->where('bounced', '0')->where('complaint', '0')->where('confirmed', '1');
          return view('lists.show', compact('list', 'subscribers', 'active'));
      } else if($request->status === 'unconfirmed') {
          $active = 'unconfirmed';
          $subscribers = $list->subscribers->where('confirmed', 0);
          return view('lists.show', compact('list', 'subscribers', 'active'));
      } else if($request->status === 'unsubscribed') {
          $active = 'unsubscribed';
          $subscribers = $list->subscribers->where('unsubscribed', 1);
          return view('lists.show', compact('list', 'subscribers', 'active'));
      } else if($request->status === 'bounced') {
          $active = 'bounced';
          $subscribers = $list->subscribers->where('bounced', 1);
          return view('lists.show', compact('list', 'subscribers', 'active'));
      } else if($request->status === 'complaint') {
          $active = 'complaint';
          $subscribers = $list->subscribers->where('complaint', 1);
          return view('lists.show', compact('list', 'subscribers', 'active'));
      }

      $active = 'all';
      $subscribers = $list->subscribers;
      return view('lists.show', compact('list', 'subscribers', 'active'));
    }

    public function edit($id)
    {
        $list = SubscriberList::findOrFail($id);
        return view('lists.edit', compact('list'));
    }

    public function update(ListRequest $request, $id)
    {
      $lists = SubscriberList::findOrFail($id);
      $lists->name = $request->name;
      $lists->save();
      return redirect()->action('ListController@index')->with('status', 'Список успешно изменен');
    }

    public function addSub(SubscriberList $list)
    {
        $id = $list->id;
        $subscribers = Subscriber::orWhereDoesntHave('lists', function($query) use ($id) {
            $query->where('list_id', $id);
        })->get();
        return view('lists.subs.create', compact('list', 'subscribers'));
    }

    public function attachSub($lid, $sid)
    {
        $subscriber = Subscriber::findOrFail($sid);
        $subscriber->lists()->attach($lid);
        return redirect()->action('ListController@show', $lid)->with('status', 'Подписчик успешно добавлен в список');
    }

    public function detachSub($lid, $sid)
    {
        $subscriber = Subscriber::findOrFail($sid);
        $subscriber->lists()->detach($lid);
        return redirect()->route('lists.show', $lid)->with('status', 'Подписчик успешно удален из списка');
    }

    public function action(Request $request, $lid)
    {
        $msg = [
          'required' => 'Необходимо отметить хотя бы одного'
        ];
        $request->validate([
            'subscriber' => 'required'
        ],$msg);

        if($request->button === 'unsubscribe'){
            $subsids = $request->subscriber;
            foreach ($subsids as $sid) {
              $subscriber = Subscriber::findOrFail($sid);
              $subscriber->unsubscribed = 1;
              $subscriber->save();
            }
            return redirect()->back()->with('status', 'Отмеченные подписчики были отписаны');
        } else if($request->button === 'detach') {
            $subsids = $request->subscriber;
            foreach ($subsids as $sid) {
              $subscriber = Subscriber::findOrFail($sid);
              $subscriber->lists()->detach($lid);
            }
            return redirect()->back()->with('status', 'Отмеченные подписчики успешно удален из списка');
        } else {
            return back();
        }
    }

    public function destroy(SubscriberList $list)
    {
      $list->delete();
      return redirect()->route('lists.index')->with('status', 'Удаление прошло успешно.');
    }
}
