<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Template;

class TemplateController extends Controller
{

    public function index(){
    	$users = Template::select(['id','name']);

      return Datatables::of($users)->make();
    }

    public function getTemplate(Request $request)
    {
      $templates = Template::where('id', $request->tem_id)->pluck('body');
      return response()->json($templates);
    }

    public function fileUploade(Request $request)
    {
      $CKEditor = $request->CKEditor;
      $funcNum = $request->CKEditorFuncNum;
      $message = $url = '';
      if ($request->hasFile('upload')) {
          $file = $request->file('upload');
          if ($file->isValid()) {
              $filename = time().'-'.$file->getClientOriginalName();
              $file->move(public_path().'/img/', $filename);
              $url = route('home').'/img/'.$filename;
          } else {
              $message = 'An error occured while uploading the file.';
          }
      } else {
          $message = 'No file uploaded.';
      }
      echo '<script>window.parent.CKEDITOR.tools.callFunction('.$funcNum.', "'.$url.'", "'.$message.'")</script>';
    }

}
