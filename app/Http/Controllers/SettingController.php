<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SettingCampaignRequest;
use Brotzka\DotenvEditor\DotenvEditor;
use App\Settings;
use App\User;
use Auth;
use Hash;
use Helper;
use AWS;

class SettingController extends Controller
{
    public function index(){
        $company = array();
        $company['company_name'] = Settings::get('company_name');
        $company['from_name'] = Settings::get('from_name');
        $company['from_email'] = Settings::get('from_email');
        $company['reply_email'] = Settings::get('reply_email');
        $regions = [
          '1'=>[
            'name'=>'US West (Oregon)',
            'value'=> 'us-west-2'
          ],
          '2'=>[
            'name'=>'US East (N. Virginia)',
            'value'=> 'us-east-2'
          ],
          '3'=>[
            'name'=>'EU (Ireland)',
            'value'=> 'eu-west-1'
          ]
        ];

        return view('settings.index', compact('company','regions'));
    }

    public function mainSettingStore(SettingCampaignRequest $request)
    {
        Settings::set('company_name', $request->company_name);
        Settings::set('from_name', $request->from_name);
        Settings::set('from_email', $request->from_email);
        Settings::set('reply_email', $request->reply_email);
        return redirect()->back()->with('status', 'Успешно Сохранен');
    }

    public function mailSettingStore(Request $request)
    {
        $env = new DotenvEditor();
        $env->changeEnv([
          'SES_KEY'   => $request->key,
          'SES_SECRET'   => $request->secret,
          'SES_REGION' => $request->region
        ]);
        return redirect()->back()->with(['status'=>'Успешно Сохранен', 'show' => 'mail']);
    }

    public function personalSettingStore(Request $request)
    {
        $msg = [
          'required' => 'Это поле обязательно для заполнения'
        ];
        $request->validate([
            'email' => 'required|email',
            'name' => 'required|string',
        ],$msg);
        $user = User::findOrFail(Auth::user()->id);
        $user->email = $request->email;
        $user->name = $request->name;
        if ($request->old_password && $request->password) {
          if(!Hash::check($request->old_password, $user->password)){
            return back()->with(['error' => 'Указанный пароль не совпадает с паролем базы данных', 'show' => 'personal']);
          }else{
            $user->password = bcrypt($request->password);
          }
        }
        $user->save();
        return redirect()->back()->with(['status'=>'Успешно Сохранен', 'show' => 'personal']);
    }

    public function checkEmailVerification(Request $request)
    {
        $validator = $request->validate([
            'from_email' => 'required|email',
        ]);
        $from_email = isset($request->from_email) ? $request->from_email : '';
        $auto_verify = $request->auto_verify=='no' ? false : true;
        //Get email's domain
        $from_email_domain_array = explode('@', $from_email);
	      $from_email_domain = $from_email_domain_array[1];
        if (!empty(config('services.ses.key')) && !empty(config('services.ses.secret'))) {
          $ses = AWS::createClient('ses');
          $v_addresses = $ses->listVerifiedEmailAddresses([]);
          if(!$v_addresses){
      			echo 'api error';
      		} else {
            $verifiedEmailsArray = array();
            $verifiedDomainsArray = array();

            foreach($v_addresses['VerifiedEmailAddresses'] as $val){
              array_push($verifiedEmailsArray, $val);
            }

            $veriStatus = true;
            $getIdentityVerificationAttributes = $ses->getIdentityVerificationAttributes([
                'Identities' => [$from_email], // REQUIRED
            ]);
            // dd($getIdentityVerificationAttributes);
            foreach($getIdentityVerificationAttributes['VerificationAttributes'] as $getIdentityVerificationAttribute)
                if($getIdentityVerificationAttribute['VerificationStatus']=='Pending') $veriStatus = false;
            if(!in_array($from_email, $verifiedEmailsArray) && !in_array($from_email_domain, $verifiedDomainsArray)){
              //Attempt to verify the email address, a verification email will be sent to the 'From email' address by Amazon SES
              if($auto_verify)
                $ses->verifyEmailAddress($from_email);
              echo 'unverified'; //From email address or domain IS NOT verified in SES console
            }
            else if(!$veriStatus)
              echo 'pending verification'; //From email address or domain is 'pending verification' in SES console
            else
              echo 'verified'; //From email address or domain IS verified in SES console
          }
        }

    }
}
