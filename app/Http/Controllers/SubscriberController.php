<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSubscriberRequest;
use App\Http\Requests\UpdateSubscriberRequest;
use Illuminate\Http\Request;
use App\Subscriber;
use App\SubscriberList;
use Excel;
class SubscriberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( )
    {
      $subscribers = Subscriber::with('lists')->paginate(20);
      return view('subscribers.index')->withSubscribers($subscribers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lists = SubscriberList::all();
        return view('subscribers.create')->withLists($lists);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSubscriberRequest $request)
    {
        $subscriber = new Subscriber;
        $subscriber->name = $request->name;
        $subscriber->email = $request->email;
        $subscriber->save();
        $lists = $request->lists;
        if (!empty($lists)) {
          $subscriber->lists()->attach($lists);
        }

        return redirect()->back()->with('status', 'Подписчик успешно добавлен');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Subscriber $subscriber)
    {
      $lists = SubscriberList::all();
      return view('subscribers.edit')->withLists($lists)->withSubscriber($subscriber);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSubscriberRequest $request, Subscriber $subscriber)
    {
      $subscriber->name = $request->name;
      $subscriber->email = $request->email;
      $subscriber->save();
      if (isset($request->lists)) {
        $subscriber->lists()->sync($request->lists);
      }else{
        $subscriber->lists()->sync(array());
      }
      return redirect()->action('SubscriberController@index')->with('status', 'Изменение успешно сохранены');
    }

    public function unSubscribe(Subscriber $subscriber)
    {
        $subscriber->unsubscribed = 1;
        $subscriber->save();
        return redirect()->back();
    }

    public function subscribe(Subscriber $subscriber)
    {
        $subscriber->unsubscribed = 0;
        $subscriber->confirmed = 1;
        $subscriber->save();
        return redirect()->back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subscriber $subscriber)
    {
        $subscriber->delete();
        return response()->json([
          'message' => 'Удаление прошло успешно.',
          'success' => true
        ]);
    }

    public function excelImport(Request $request)
    {
      $this->validate($request,[
        'file'=>'required|mimes:xlsx,xls'
      ]);

      if($request->hasFile('file'))
      {
          $path = $request->file('file')->getRealPath();
          $data = Excel::load($path, function($reader){})->get();
          if(!empty($data) && $data->count()){
            foreach ($data as $key => $value) {
              $subscriber = new Subscriber();
              $subscriber->name = $value->name;
              $subscriber->email = $value->email;
              $subscriber->save();
            }
          }
      }

      return back();
    }
}
