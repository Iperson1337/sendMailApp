<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Link;
use App\Campaign;
use App\Subscriber;
use AWS;
use Aws\Sns\Message;
use Aws\Sns\MessageValidator;
use Aws\Sns\Exception\InvalidSnsMessageException;
use Illuminate\Support\Facades\Storage;

class MailTrackerController extends Controller
{

    public function click($sid, $lid, $cid)
    {
      $subID = decrypt($sid);
      $link_id = decrypt($lid);
      $campaign_id = decrypt($cid);

      $link = Link::findOrFail($link_id);
      if(empty($link->clicks))
        $val = $subID;
      else
      {
        $link->clicks .= ','.$subID;
        $val = $link->clicks;
      }
      $link->clicks = $val;
      $link->save();

      $campaign = Campaign::findOrFail($campaign_id);
      if(count($campaign) > 0){
        $opened = false;
        $opens = $campaign->opens;
        $opens_array = explode(',', $opens);
        foreach($opens_array as $open)
        {
          if($open == $subID) $opened = true;
        }
      }
      if(!$opened){
        $this->open($cid, $sid);
      }
      return redirect($link->link);
    }

    public function open($cid,$sid)
    {
        $campaign_id = decrypt($cid);
        $subID = decrypt($sid);
        $campaign = Campaign::findOrFail($campaign_id);
        if(empty($campaign->opens))
          $val = $subID;
        else
        {
          $campaign->opens .= ','.$subID;
          $val = $campaign->opens;
        }

        $campaign->opens = $val;
        $campaign->save();

        $subscriber = Subscriber::where('id',$subID)->where('bounced', 1)->first();
        if(!is_null($subscriber)){
          $subscriber->bounced = '0';
          $subscriber->last_campaign = $campaign_id;
          $subscriber->save();
        }

        return redirect(asset('img/to.png'));
    }

    public function unsubscribe($subsemail, $cid)
    {
      $email = decrypt($subsemail);
      $camID = decrypt($cid);
      $subscribers = Subscriber::where('email', 'like', $email)->get();
      foreach ($subscribers as $key => $subscriber) {
        $subscriber->unsubscribed = 1;
        $subscriber->last_campaign = $camID;
        $subscriber->save();
      }
      $link = route('resubscribe',[$subsemail, $cid]);
      return view('subscribers.unsubscribe', compact('link'));
    }

    public function resubscribe($subsemail, $cid)
    {
      $email = decrypt($subsemail);
      $camID = decrypt($cid);
      $subscribers = Subscriber::where('email', 'like', $email)->get();
      foreach ($subscribers as $key => $subscriber) {
        $subscriber->unsubscribed = 0;
        $subscriber->last_campaign = $camID;
        $subscriber->save();
      }
      return view('subscribers.subscribe');
    }

    public function test()
    {
        $jsonComplaint = '{"notificationType":"Complaint","complaint":{"complainedRecipients":[{"emailAddress":"complaint Spam <mukat.alibi.21.97@gmail.com>"}],"timestamp":"2018-05-16T20:14:25.000Z","feedbackId":"010201636a96ede1-b2751646-5c92-4651-b87a-4b6f512d921b-000000","userAgent":"Amazon SES Mailbox Simulator","complaintFeedbackType":"abuse"},"mail":{"timestamp":"2018-05-16T20:14:24.000Z","source":"laravel@kakazstandart.kz","sourceArn":"arn:aws:ses:eu-west-1:905126057231:identity/laravel@kakazstandart.kz","sourceIp":"195.210.46.63","sendingAccountId":"905126057231","messageId":"010201636a96e5ad-687a14d0-7686-44e4-a91b-52e33e189e6e-000000","destination":["complaint Spam <complaint@simulator.amazonses.com>"]}}';
        $jsonBounced = '{"notificationType":"Bounce","bounce":{"bounceType":"Permanent","bounceSubType":"General","bouncedRecipients":[{"emailAddress":"Bounce <mukat.alibi.21.97@gmail.com>","action":"failed","status":"5.1.1","diagnosticCode":"smtp; 550 5.1.1 user unknown"}],"timestamp":"2018-05-16T17:43:04.400Z","feedbackId":"010201636a0c59b7-2c27eb6b-6b37-410e-97ff-3cade3bbe40d-000000","remoteMtaIp":"72.21.215.231","reportingMTA":"dsn; a7-32.smtp-out.eu-west-1.amazonses.com"},"mail":{"timestamp":"2018-05-16T17:43:03.000Z","source":"laravel@kakazstandart.kz","sourceArn":"arn:aws:ses:eu-west-1:905126057231:identity/laravel@kakazstandart.kz","sourceIp":"195.210.46.63","sendingAccountId":"905126057231","messageId":"010201636a0c5617-2c9ba4c8-c28f-4138-a3c9-4ee9e0039603-000000","destination":["Bounce <bounce@simulator.amazonses.com>"]}}';


        $responseMessage = json_decode($jsonComplaint, JSON_BIGINT_AS_STRING);

        $complaint_simulator_email = 'complaint@simulator.amazonses.com';
        $bounce_simulator_email = 'bounce@simulator.amazonses.com';

        if ($responseMessage['notificationType'] == 'Complaint'){
            $problem_email = $responseMessage['complaint']['complainedRecipients'];
            $problem_email = $this->get_email($problem_email[0]['emailAddress']);
            $from_email = $this->get_email($responseMessage['mail']['source']);
            $subscriber = Subscriber::where('email', $problem_email)->first();
            if ($problem_email==$complaint_simulator_email) {
              if (filter_var($from_email, FILTER_VALIDATE_EMAIL)){
                $campaigns = Campaign::where('from_email', $from_email)->get();
                foreach ($campaigns as $campaign) {
                  $campaign->update([
                    'complaint_setup' => '1'
                  ]);
                }
              }
            } else if (!empty($subscriber)) {
              $subscriber->bounced = 0;
              $subscriber->unsubscribed = 0;
              $subscriber->complaint = 1;
              $subscriber->save();
            }
        }

        if ($responseMessage['notificationType'] == 'Bounce') {
            $problem_email = $responseMessage['bounce']['bouncedRecipients'];
            $problem_email = $this->get_email($problem_email[0]['emailAddress']);
            $from_email = $this->get_email($responseMessage['mail']['source']);
            $subscriber = Subscriber::where('email', $problem_email)->first();
            if ($problem_email==$bounce_simulator_email) {
              if (filter_var($from_email, FILTER_VALIDATE_EMAIL)){
                $campaigns = Campaign::where('from_email', $from_email)->get();
                foreach ($campaigns as $campaign) {
                  $campaign->update(['bounce_setup' => '1']);
                }
              }
            } else if (!empty($subscriber)) {
              if ($responseMessage['bounce']['bounceType'] == 'Transient') {
                if($subscriber->bounce_soft >= 3){
                  $subscriber->update(['bounced' => '1']);
                }else{
                  $subscriber->bounce_soft = $subscriber->bounce_soft+1;
                  $subscriber->save();
                }
              } else if ($responseMessage['bounce']['bounceType'] == 'Permanent'){
                $subscriber->update(['bounced' => '1']);
              }
            }
        }

    }

    public function bounced()
    {
        $message = Message::fromRawPostData();
        $validator = new MessageValidator();
        $responseMessage = json_decode($message['Message'], JSON_BIGINT_AS_STRING);
        try {
            $validator->validate($message);
        } catch (InvalidSnsMessageException $e) {
            http_response_code(404);
            error_log('SNS Message Validation Error: ' . $e->getMessage());
            die();
        }

        if ($message['Type'] === 'SubscriptionConfirmation') {
            Storage::append('AWS_SNS.log', 'SubscribeURL'.$message['SubscribeURL']);
            file_get_contents($message['SubscribeURL']);
        }

        if ($message['Type'] != 'Notification') {
            Storage::append('AWS_SNS.log', 'not notification');
            return;
        }

        if ($responseMessage['notificationType'] == 'AmazonSnsSubscriptionSucceeded') {
            Storage::append('AWS_SNS.log', 'subscription confirmed by AWS');
            return;
        }

        if ($responseMessage['notificationType'] == 'Bounce') {
            $problem_email = $responseMessage['bounce']['bouncedRecipients'];
            $problem_email = $this->get_email($problem_email[0]['emailAddress']);
            $from_email = $this->get_email($responseMessage['mail']['source']);
            $subscriber = Subscriber::where('email', $problem_email)->first();
            if ($problem_email==$bounce_simulator_email) {
                if (filter_var($from_email, FILTER_VALIDATE_EMAIL)){
                    $campaigns = Campaign::where('from_email', $from_email)->get();
                    foreach ($campaigns as $campaign) {
                      $campaign->update(['bounce_setup' => '1']);
                    }
                }
            } else if (!empty($subscriber)) {
                if ($responseMessage['bounce']['bounceType'] == 'Transient') {
                    if($subscriber->bounce_soft >= 3){
                      $subscriber->update(['bounced' => '1']);
                    }else{
                      $subscriber->bounce_soft = $subscriber->bounce_soft+1;
                      $subscriber->save();
                    }
                } else if ($responseMessage['bounce']['bounceType'] == 'Permanent'){
                    $subscriber->update(['bounced' => '1']);
                }
            }
        }
    }

    public function complaint()
    {
        $message = Message::fromRawPostData();
        $validator = new MessageValidator();
        $responseMessage = json_decode($message['Message'], JSON_BIGINT_AS_STRING);
        $complaint_simulator_email = 'complaint@simulator.amazonses.com';
        // Validate the message and log errors if invalid.
        try {
          $validator->validate($message);
        } catch (InvalidSnsMessageException $e) {
          // Pretend we're not here if the message is invalid.
          http_response_code(404);
          error_log('SNS Message Validation Error: ' . $e->getMessage());
          die();
        }
        // Check the type of the message and handle the subscription.
        if ($message['Type'] === 'SubscriptionConfirmation') {
          // Confirm the subscription by sending a GET request to the SubscribeURL
          Storage::append('AWS_SNS.log', 'Complaint SubscribeURL'.$message['SubscribeURL']);
          file_get_contents($message['SubscribeURL']);
        }

        if ($message['Type'] != 'Notification') {
            Storage::append('AWS_SNS.log', 'not notification Complaint');
            return;
        }

        if ($responseMessage['notificationType'] == 'AmazonSnsSubscriptionSucceeded') {
          Storage::append('AWS_SNS.log', 'subscription confirmed by AWS');

          return;
        }

        if ($responseMessage['notificationType'] == 'Complaint'){
            $problem_email = $responseMessage['complaint']['complainedRecipients'];
            $problem_email = $this->get_email($problem_email[0]['emailAddress']);
            $from_email = $this->get_email($responseMessage['mail']['source']);
            $subscriber = Subscriber::where('email', $problem_email)->first();
            if ($problem_email==$complaint_simulator_email) {
                if (filter_var($from_email, FILTER_VALIDATE_EMAIL)){
                    $campaigns = Campaign::where('from_email', $from_email)->get();
                    foreach ($campaigns as $campaign) {
                      $campaign->update([
                        'complaint_setup' => '1'
                      ]);
                    }
                }
            } else if (!empty($subscriber)) {
                $subscriber->bounced = 0;
                $subscriber->unsubscribed = 0;
                $subscriber->complaint = 1;
                $subscriber->save();
            }
        }
    }

    private function get_email($string)
    {
        foreach(preg_split('/\s/', $string) as $token)
        {
            $email = filter_var(filter_var($token, FILTER_SANITIZE_EMAIL), FILTER_VALIDATE_EMAIL);
            if ($email !== false) $emails[] = $email;
        }
        return $emails[0];
    }


}
