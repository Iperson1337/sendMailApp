<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreCampaignRequest;
use App\Http\Requests\SendRequest;
use App\Jobs\SendCampaignMails;
use App\Mail\SendCampaign;
use App\Campaign;
use App\Settings;
use App\Template;
use App\Subscriber;
use App\SubscriberList;
use App\Link;
use Carbon\Carbon;
use Mail;
use Helper;
use AWS;

class CampaignController extends Controller
{
    public function index()
    {
      $campaigns = Campaign::with('links')->get();
      $opens = array();
      $percentage_clicked = array();
      foreach ($campaigns as $key => $campaign) {
        if (empty($campaign->opens)){
          $opens[$campaign->id]['unique'] = '0';
          $opens[$campaign->id]['all'] = '0';
          $opens[$campaign->id]['per'] = '0';
        }
        else {
          $opens[$campaign->id]['unique'] = count(array_unique(explode(',', $campaign->opens)));
          $opens[$campaign->id]['all'] = count(explode(',', $campaign->opens));
          $opens[$campaign->id]['per'] = round($opens[$campaign->id]['unique']/($campaign->recipients-Subscriber::get_bounced_count($campaign->id)) * 100, 2);
        }

        if(empty($opens['all']))
            $opens[$campaign->id]['all'] = '0';

        if($campaign->recipients==0 || empty($campaign->recipients))
            $percentage_clicked[$campaign->id] = round(Campaign::get_click_percentage($campaign->id) *100, 2);
        else
            $percentage_clicked[$campaign->id] = round(Campaign::get_click_percentage($campaign->id)/$campaign->recipients *100, 2);
      }
      return view('campaigns.index',compact('campaigns', 'percentage_clicked', 'opens'));
    }

    public function report()
    {
      $campaigns = Campaign::with('links')->where('status', Campaign::STATUS_DONE)->get();
      $opens = array();
      $percentage_clicked = array();
      foreach ($campaigns as $key => $campaign) {
        if (empty($campaign->opens)){
          $opens[$campaign->id]['unique'] = '0';
          $opens[$campaign->id]['all'] = '0';
          $opens[$campaign->id]['per'] = '0';
        }
        else {
          $opens[$campaign->id]['unique'] = count(array_unique(explode(',', $campaign->opens)));
          $opens[$campaign->id]['all'] = count(explode(',', $campaign->opens));
          $opens[$campaign->id]['per'] = round($opens[$campaign->id]['unique']/($campaign->recipients-Subscriber::get_bounced_count($campaign->id)) * 100, 2);
        }

        if(empty($opens['all']))
          $opens[$campaign->id]['all'] = '0';

        if($campaign->recipients==0 || empty($campaign->recipients))
            $percentage_clicked[$campaign->id] = round(Campaign::get_click_percentage($campaign->id) *100, 2);
        else
            $percentage_clicked[$campaign->id] = round(Campaign::get_click_percentage($campaign->id)/$campaign->recipients *100, 2);
      }
      return view('campaigns.reports', compact('campaigns', 'percentage_clicked', 'opens'));
    }

    //Get template through AJAX for campaign
    public function getTemplate(Request $request)
    {
      $templates = Template::where('id', $request->tem_id)->pluck('body');
      return response()->json($templates);
    }

    public function dublicate(Campaign $campaign)
    {
        $dublicate = $campaign->replicate();
        $dublicate->to_send = null;
        $dublicate->recipients = null;
        $dublicate->send_at = null;
        $dublicate->run_date = null;
        $dublicate->status = 'draft';
        $dublicate->save();
        return redirect()->back();
    }

    public function create()
    {
        $default = array();
        $default['company_name'] = Settings::get('company_name');
        $default['from_name'] = Settings::get('from_name');
        $default['from_email'] = Settings::get('from_email');
        $default['reply_email'] = Settings::get('reply_email');
        $templates = Template::all();
        return view('campaigns.create',compact('templates','default'));
    }

    public function store(StoreCampaignRequest $request)
    {
        $campaign = new Campaign;
        $campaign->name = $request->name;
        $campaign->subject = $request->subject;
        $campaign->from_name = $request->from_name;
        $campaign->from_email = $request->from_email;
        $campaign->plain_text = $request->plain_text;
        $campaign->reply_to = $request->reply_email;
        $campaign->query_string = $request->query_string;
        $campaign->html_text = $request->body;
        $campaign->save();
        $campaign->draft();
        // return response()->json(['success' => true,'URL'=> action('CampaignController@sendView', $campaign->id)]);
        if($request->submit === 'save'){
          return redirect('/')->with('status', 'Рассылка сохранено');
        }else{
          return redirect()->action('CampaignController@sendView', $campaign->id);
        }

    }

    public function show(Campaign $campaign)
    {
      if ($campaign->send_at || $campaign->run_at) {
        $campaignOpen = array();
        $subscriberCount = array();
        $subscriberPercentage = array();
        $subscriberCount['bounced'] = Subscriber::get_bounced_count($campaign->id);
        $subscriberCount['unsubscribes'] = Subscriber::get_unsubscribes_count($campaign->id);
        $subscriberCount['complaints'] = Subscriber::get_complaints_count($campaign->id);
        $subscriberPercentage['unsubscribe'] = round($subscriberCount['unsubscribes']/($campaign->recipients-$subscriberCount['bounced']) * 100, 4);
        $subscriberPercentage['bounced'] = round(($subscriberCount['bounced']/$campaign->recipients) * 100, 2);
        $subscriberPercentage['complaint'] = round(($subscriberCount['complaints']/$campaign->recipients) * 100, 2);
        if (empty($campaign->opens)) {
          $campaignOpen['unique'] = '0';
          $campaignOpen['all'] = '0';
          $campaignOpen['percentage'] = '0';
        } else {
          $campaignOpen['unique'] = count(array_unique(explode(',', $campaign->opens)));
          $campaignOpen['all'] = count(explode(',', $campaign->opens));
          $campaignOpen['percentage'] = round($campaignOpen['unique']/($campaign->recipients-$subscriberCount['bounced']) * 100, 2);
        }
        $campaignOpen['click'] = Campaign::get_click_percentage($campaign->id);
        $campaignOpen['click_percentage'] = round($campaignOpen['click']/($campaign->recipients-$subscriberCount['bounced']) *100, 4);
        $campaignOpen['not'] = $campaign->recipients-$campaignOpen['unique']; //кол-во не открытых писем

        //Получение уникальных подписчиков из $campaign->opens
        $collection = array();
        $last_opens_array = explode(',', $campaign->opens);
        $loop_no = count(array_unique($last_opens_array));
        if($loop_no>10) $loop_no = 10;
        for($z=0;$z<$loop_no;$z++){
          $last_opens_array2 = array_reverse(array_unique($last_opens_array));
          $last_subscriber_id = explode(':', $last_opens_array2[$z]);
          $subs = Subscriber::where('id', $last_subscriber_id[0])->get();
          foreach($subs as $sub){
            $collection[$sub->id] = $sub;
          }
        }
        $subscribers = collect($collection);

        return view('campaigns.show', compact('campaign', 'campaignOpen', 'subscriberCount', 'subscriberPercentage', 'subscribers'));
      }
      else if(is_null($campaign->send_at) || is_null($campaign->run_at)){
        return redirect()->action('CampaignController@sendView', $campaign->id);
      }
    }

    public function preview(Campaign $campaign)
    {
        return view('campaigns.preview',compact('campaign'));
    }

    public function edit(Campaign $campaign)
    {
        $templates = Template::all();
        return view('campaigns.edit',compact('campaign','templates'));
    }

    public function update(StoreCampaignRequest $request, Campaign $campaign)
    {
      $campaign->name = $request->name;
      $campaign->subject = $request->subject;
      $campaign->from_name = $request->from_name;
      $campaign->from_email = $request->from_email;
      $campaign->plain_text = $request->plain_text;
      $campaign->reply_to = $request->reply_email;
      $campaign->query_string = $request->query_string;
      $campaign->html_text = $request->body;
      $campaign->save();
      if($request->submit === 'save'){
        return redirect('/')->with('status', 'Изменения сохранены');
      }else{
        return redirect()->action('CampaignController@sendView', $campaign->id);
      }
    }

    public function destroy(Campaign $campaign)
    {
        $campaign->delete();
        return redirect('/')->with('status', 'Удаление прошло успешно.');
    }

    public function sendView(Campaign $campaign)
    {
        $ses = AWS::createClient('ses')->getSendQuota([]);
        $lists = SubscriberList::all();
        return view('campaigns.send',compact('campaign','lists', 'ses'));
    }

    public function testsend(Request $request, Campaign $campaign)
    {
          $toemail = $request->email;
          Mail::send('mail.test', ['campaign' => $campaign,'toemail' => $request->email], function($message) use ($campaign, $toemail){
            $site_mail = $campaign->from_email;
            $site_name = $campaign->from_name;
            $message->from($site_mail, $site_name);
            $message->to($toemail, $toemail)->subject($campaign->subject);
          });
          return redirect()->action('CampaignController@sendView', $campaign->id)->with('status', 'Тестовый письмо отправлен');
    }

    public function send(SendRequest $request, Campaign $campaign)
    {
        $lists = $request->lists;
        $campaign->lists()->attach($lists);
        if ($request->submit === 'now'){
            $campaign->sending();
            Helper::parseUrl($campaign->id);
            dispatch(new SendCampaignMails($campaign));
            return redirect()->action('CampaignController@index')->with('status', 'Рассылка запущена');
        } else {
            $messages = [
              'time.required'=> "Нужно выбрать время",
              'date.required'=> "Нужно выбрать дату"
            ];
            $this->validate($request, [
              'date' => 'required',
              'time' => 'required'
            ],$messages);

            $datetime = Carbon::createFromFormat('Y-m-d H:i', $request->date.' '.$request->time);
            $campaign->queue($datetime);
            Helper::parseUrl($campaign->id);
            $job = (new SendCampaignMails($campaign))->delay($campaign->run_date);
            dispatch($job);
            return redirect()->action('CampaignController@index')->with('status', 'Рассылка запланирован');
        }


    }

    public function progress(Request $request)
    {
        //get send count
        $campaign = Campaign::select('to_send', 'recipients')->where('id', $request->campaign_id)->first();
        $percentage = $campaign->recipients / $campaign->to_send * 100;
        if($campaign->to_send == $campaign->recipients)
        {
          return $campaign->recipients;
        }
        else
        {
          return $campaign->recipients.' <span style="color:#488846;">('.round($percentage).'%)</span> <img src="'.asset('img/loader.gif').'" style="width:16px;"/>';
        }
    }

}
