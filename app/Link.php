<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $table='links';
    protected $fillable = [
      'link',
      'clicks'
    ];
    public function campaigns()
    {
        return $this->belongsToMany('App\Campaign', 'campaign_links');
    }
}
