<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Campaign;
use App\Link;
use Mail;
use App\Mail\SendCampaign;
use App\Subscriber;
use App\SubscriberList;
use Helper;
use Illuminate\Support\Facades\Storage;

class SendCampaignMails implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $campaign;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Campaign $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->campaign->sending();
        $collection = array();
        foreach ($this->campaign->lists as $list) {
          $subs = $list->subscribers->where('unsubscribed', '0')->where('bounced', '0')->where('complaint', '0')->where('confirmed', '1');
          foreach ($subs as $subscriber) {
            $collection[$subscriber->id] = $subscriber;
          }
        }
        $subscribers = collect($collection);
        $this->campaign->update([
            'to_send' => $subscribers->count(),
        ]);
        foreach ($subscribers as $subscriber) {
            $subscriber->update([ 'last_campaign'=> $this->campaign->id ]);
            Mail::to($subscriber->email, $subscriber->name)->queue(new SendCampaign($this->campaign, $subscriber));
            $this->campaign->update([ 'recipients'=> $this->campaign->recipients + 1 ]);
        }
        $this->campaign->done();
    }
}
