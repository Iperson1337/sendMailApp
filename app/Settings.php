<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    public static function get($name)
    {
        $setting = self::where('name', $name)->first();
        if (is_object($setting)) {
            return $setting->value;
        } else {
            return self::defaultSettings()[$name]['value'];
        }
    }

    public static function set($name, $val)
    {
        $option = self::where('name', $name)->first();

        if (is_object($option)) {
            $option->value = $val;
        } else {
            $option = new self();
            $option->name = $name;
            $option->value = $val;
        }
        $option->save();

        return $option;
    }

    public static function defaultSettings()
    {
        return [
            'company_name' => [
                'value' => 'NewsLetter',
            ],
            'from_name' => [
                'value' => 'NewsLetter',
            ],
            'from_email' => [
                'value' => 'laravel@kakazstandart.kz',
            ],
            'reply_email' => [
                'value' => 'laravel@kakazstandart.kz',
            ],
        ];
    }

}
