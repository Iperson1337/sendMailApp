<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::middleware('auth')->group(function(){
  Route::get('/', 'CampaignController@index')->name('home');

  Route::resource('/templates', 'TemplateController');

  Route::resource('/campaigns', 'CampaignController');
  Route::get('/campaigns/{campaign}/preview', 'CampaignController@preview')->name('campaigns.preview');
  Route::get('/campaigns/{campaign}/dublicate', 'CampaignController@dublicate')->name('campaigns.dublicate');
  Route::get('/campaigns/{campaign}/send', 'CampaignController@sendView')->name('campaigns.sendview');
  Route::post('/campaigns/{campaign}/send', 'CampaignController@send')->name('campaigns.send');
  Route::post('/campaigns/{campaign}/test', 'CampaignController@testsend')->name('campaigns.testsend');
  Route::get('/reports', 'CampaignController@report')->name('campaigns.reports');

  Route::resource('/subscribers', 'SubscriberController');
  Route::get('/unsubs/{subscriber}', 'SubscriberController@unSubscribe')->name('subs.unsubscribe');
  Route::get('/subscribe/{subscriber}', 'SubscriberController@subscribe')->name('subs.subscribe');
  Route::post('/subscribers/import', 'SubscriberController@excelImport')->name('subs.import');

  Route::resource('/lists', 'ListController');
  Route::post('/massaction/{list}', 'ListController@action')->name('mass.action');
  Route::get('/subs/{list}', 'ListController@addSub')->name('subs.add');
  Route::get('/subs/{list}/{subscriber}/a', 'ListController@attachSub')->name('subs.attach');
  Route::get('/subs/{list}/{subscriber}/d', 'ListController@detachSub')->name('subs.detach');

  Route::get('/settings', 'SettingController@index')->name('settings');
  Route::post('/settings/main', 'SettingController@mainSettingStore')->name('settings.main');
  Route::post('/settings/mail', 'SettingController@mailSettingStore')->name('settings.mail');
  Route::post('/settings/personal', 'SettingController@personalSettingStore')->name('settings.personal');
  Route::post('/emailverification', 'SettingController@checkEmailVerification')->name('email.verification');
});

Route::prefix('api')->namespace('API')->middleware('auth')->group(function(){
  Route::get('get-template','TemplateController@getTemplate')->name('get.template');
  Route::get('templates', 'TemplateController@index')->name('api.templates');
  Route::post('upload', 'TemplateController@fileUploade')->name('file.upload');
});

//Tracker NO Auth
Route::get('/i/{subscriber}/{link}/{campaign}', 'MailTrackerController@click')->name('link.click');
Route::get('/o/{campaign}/{subscriber}', 'MailTrackerController@open')->name('open.tracker');
Route::get('/unsubscribe/{subscriber}/{campaign}', 'MailTrackerController@unsubscribe')->name('unsubscribe');
Route::get('/resubscribe/{subscriber}/{campaign}', 'MailTrackerController@resubscribe')->name('resubscribe');
Route::get('/test-complaint-bounce', 'MailTrackerController@test')->name('complaint.bounced');

Route::post('/bounces', 'MailTrackerController@bounced')->name('bounces');
Route::post('/complaints', 'MailTrackerController@complaint')->name('complaints');
Route::post('/campaigns/progress', 'CampaignController@progress')->name('campaigns.progress');
